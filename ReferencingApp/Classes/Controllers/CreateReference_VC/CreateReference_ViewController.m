//  CreateReference_ViewController.m
//  Harvard
//  Created by appentus technologies pvt. ltd. on 10/14/19.
//  Copyright © 2019 SquaredDimesions. All rights reserved.



#import "CreateReference_ViewController.h"

#import "SegmentTableViewCell.h"
#import "AppDelegate.h"
#import "ProjectReferencesTableViewController.h"
#import <ActionSheetPicker-3.0/ActionSheetPicker.h>

#import "WebDataManager.h"
#import "Utilities.h"
#import "Picker_VC.h"
#import "Date_Picker_ViewController.h"
//#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "JSON_Param.h"
#import "ProjectsTableViewController.h"


@import MobileCoreServices;    // only needed in iOS

@interface CreateReference_ViewController () <WebDataManagerDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>

@end

@implementation CreateReference_ViewController {
//    raja contents:-
    NSMutableArray *arr_ref;
    NSMutableArray *arr_ref_value;
    NSMutableArray *arr_full_name;
    NSMutableArray *arr_full_name_value;
    
    NSString *str_selecte_date;
    NSString *str_referenceID;
}

- (void)viewDidDisappear:(BOOL)animated {
    //    [self.tabBarController.tabBar setHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSArray *projectsArray = [[ReferencingApp sharedInstance]  getDataBaseWithName:@"Project"];
//    NSLog(@"%@",projectsArray);
    
    self.arr_Style_Picker = [[NSMutableArray alloc]init];
    
    typeDictionary = [[NSMutableDictionary alloc] init];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tbl_creat_reference.delegate = self;
    self.tbl_creat_reference.dataSource = self;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(picker_Recieve:) name:@"picker_Recieve"  object:nil];
    
    [ReferencingApp sharedInstance].delegate = self;
    
    typeData = [ReferencingApp sharedInstance].typeData;
    
    refDataDictionary  = [[NSMutableDictionary alloc] init];
    
    arr_ref = [[NSMutableArray alloc]init];
    arr_ref_value = [[NSMutableArray alloc]init];
    
    arr_full_name = [[NSMutableArray alloc]init];
    arr_full_name_value = [[NSMutableArray alloc]init];
    
    [arr_full_name addObject:@"First Name-Last Name"];
    
    for (int i=0 ; i<arr_full_name.count; i++) {
        [arr_full_name_value addObject:@"First Name-Last Name"];
    }
    
    for (NSDictionary *dict in typeData) {
        if ([[[dict objectForKey:@"name"] lowercaseString] containsString:[self.title lowercaseString]])
//        if ([[dict objectForKey:@"name"] isEqualToString:self.title])
        {
            typeDictionary = dict;
            arr_ref = [[typeDictionary objectForKey:@"fields"] mutableCopy];
            
            for (int i = 0; i<arr_ref.count; i++) {
                [arr_ref_value addObject:@""];
            }
            NSLog(@"arr_ref_value is:- %lu",(unsigned long)arr_ref_value.count);
//            [refDataDictionary setObject:@"" forKey:[typeDictionary objectForKey:@"fields"]];
            break;
        }
    }
    
    NSLog(@"arr_ref_value is:- %lu",(unsigned long)arr_ref_value.count);
    if ([self.is_Edit isEqualToString:@"1"]) {
        NSLog(@"%@",[self currentReference]);
        str_referenceID = [self.currentReference referenceID];
        self.navigationController.navigationBar.topItem.title = [self.currentReference referenceType];
        
        NSError *jsonError;
        NSData *objectData = [[self.currentReference data] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        NSLog(@"%@",json);
        NSArray *arr_data = [json objectForKey:@"data"];
        NSLog(@"%@",arr_data);
        
        NSDictionary *dict_data = [arr_data objectAtIndex:0];
        NSLog(@"%@",dict_data);
        
        NSArray *arr_author = [dict_data objectForKey:@"author"];
        
        [arr_full_name removeAllObjects];
        [arr_full_name_value removeAllObjects];
        [arr_ref_value removeAllObjects];
        
        for (NSDictionary *dict_author in arr_author) {
            NSString *str_given = [dict_author objectForKey:@"given"];
            NSString *str_family = [dict_author objectForKey:@"family"];
            NSString *str_full_name = [NSString stringWithFormat:@"%@-%@",str_family,str_given];
            [arr_full_name_value addObject:str_full_name];
            
            [arr_full_name addObject:@"First Name-Last Name"];
        }
        
        arr_ref = [[typeDictionary objectForKey:@"fields"] mutableCopy];
        
        for (int i = 0; i<arr_ref.count; i++) {
            NSString *str_fields = [arr_ref objectAtIndex:i];
            if ([str_fields.lowercaseString isEqualToString:[@"Date Published" lowercaseString]]) {
                NSDictionary *dict_issued = [dict_data objectForKey:@"issued"];
                NSArray *arr_dateparts = [dict_issued objectForKey:@"date-parts"];
                NSArray *arr_dates = [arr_dateparts objectAtIndex:0];
                NSString *str_full_date = [NSString stringWithFormat:@"%@,%@,%@",[arr_dates objectAtIndex:0],[arr_dates objectAtIndex:1],[arr_dates objectAtIndex:2]];
                
                [arr_ref_value addObject:str_full_date];
            } else if ([str_fields.lowercaseString isEqualToString:[@"Date Accessed Viewed" lowercaseString]]) {
                NSDictionary *dict_accessed = [dict_data objectForKey:@"accessed"];
                NSArray *arr_dateparts = [dict_accessed objectForKey:@"date-parts"];
                NSArray *arr_dates = [arr_dateparts objectAtIndex:0];
                NSString *str_full_date = [NSString stringWithFormat:@"%@,%@,%@",[arr_dates objectAtIndex:0],[arr_dates objectAtIndex:1],[arr_dates objectAtIndex:2]];
                
                [arr_ref_value addObject:str_full_date];
            } else if ([str_fields.lowercaseString containsString:@"title"]) {
                NSString *str_ref_value = [dict_data objectForKey:@"title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"Annotation" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"note"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"Publisher Place" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"publisher-place"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"Page Range" lowercaseString]]) {
                if ([[self.title lowercaseString] isEqualToString:[@"book chapter" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"book review" lowercaseString]]) {
                    NSString *str_ref_value = [dict_data objectForKey:@"number-of-volumes"];
                    [arr_ref_value addObject:str_ref_value];
                } else if ([[self.title lowercaseString] isEqualToString:[@"encyclopedia entry" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"journal article" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"magazine article" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"news article" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"report" lowercaseString]]) {
                    NSString *str_ref_value = [dict_data objectForKey:@"page"];
                    [arr_ref_value addObject:str_ref_value];
                } else {
                    NSString *str_ref_value = [dict_data objectForKey:@"number-of-pages"];
                    [arr_ref_value addObject:str_ref_value];
                }
            } else if ([str_fields.lowercaseString containsString:[@"Book Author" lowercaseString]]) {
                NSArray *arr_collection_editor = [dict_data objectForKey:@"collection-editor"];
                
                NSString *str_given = [[arr_collection_editor objectAtIndex:0] objectForKey:@"given"];
                NSString *str_family = [[arr_collection_editor objectAtIndex:0] objectForKey:@"family"];
                
                [arr_ref_value addObject:[NSString stringWithFormat:@"%@ %@",str_given,str_family]];
            } else if ([str_fields.lowercaseString containsString:[@"Name Of Journal Proceedings" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"container-title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"Title of Article" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"container-title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"Database name" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"source"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"Name of Dictionary" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"container-title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"Name of Encyclopedia" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"container-title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"distributor" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"publisher"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"container" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"container-title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"interviewer" lowercaseString]]) {
                NSArray *arr_collection_editor = [dict_data objectForKey:@"interviewer"];
                
                NSString *str_given = [[arr_collection_editor objectAtIndex:0] objectForKey:@"given"];
                NSString *str_family = [[arr_collection_editor objectAtIndex:0] objectForKey:@"family"];
                
                [arr_ref_value addObject:[NSString stringWithFormat:@"%@ %@",str_given,str_family]];
            } else if ([str_fields.lowercaseString isEqualToString:[@"Date of interview" lowercaseString]]) {
                NSDictionary *dict_issued = [dict_data objectForKey:@"issued"];
                NSArray *arr_dateparts = [dict_issued objectForKey:@"date-parts"];
                NSArray *arr_dates = [arr_dateparts objectAtIndex:0];
                NSString *str_full_date = [NSString stringWithFormat:@"%@,%@,%@",[arr_dates objectAtIndex:0],[arr_dates objectAtIndex:1],[arr_dates objectAtIndex:2]];
                
                [arr_ref_value addObject:str_full_date];
            } else if ([str_fields.lowercaseString containsString:[@"name of journal" lowercaseString]]) {
                [arr_ref_value addObject:[dict_data objectForKey:@"container-title"]];
            } else if ([str_fields.lowercaseString containsString:[@"volume number" lowercaseString]]) {
                [arr_ref_value addObject:[dict_data objectForKey:@"volume"]];
            } else if ([str_fields.lowercaseString containsString:[@"issue number" lowercaseString]]) {
                [arr_ref_value addObject:[dict_data objectForKey:@"issue"]];
            } else if ([str_fields.lowercaseString isEqualToString:[@"Date issued" lowercaseString]]) {
                NSDictionary *dict_issued = [dict_data objectForKey:@"issued"];
                NSArray *arr_dateparts = [dict_issued objectForKey:@"date-parts"];
                NSArray *arr_dates = [arr_dateparts objectAtIndex:0];
                NSString *str_full_date = [NSString stringWithFormat:@"%@,%@,%@",[arr_dates objectAtIndex:0],[arr_dates objectAtIndex:1],[arr_dates objectAtIndex:2]];
                
                [arr_ref_value addObject:str_full_date];
            } else if ([str_fields.lowercaseString containsString:[@"case name" lowercaseString]]) {
                [arr_ref_value addObject:[dict_data objectForKey:@"title"]];
            } else if ([str_fields.lowercaseString containsString:[@"date of decision" lowercaseString]]) {
                NSDictionary *dict_issued = [dict_data objectForKey:@"issued"];
                NSArray *arr_dateparts = [dict_issued objectForKey:@"date-parts"];
                NSArray *arr_dates = [arr_dateparts objectAtIndex:0];
                NSString *str_full_date = [NSString stringWithFormat:@"%@,%@,%@",[arr_dates objectAtIndex:0],[arr_dates objectAtIndex:1],[arr_dates objectAtIndex:2]];
                
                [arr_ref_value addObject:str_full_date];
            } else if ([str_fields.lowercaseString containsString:[@"Name of MAGAZINE" lowercaseString]] || [str_fields.lowercaseString containsString:[@"Name of news source" lowercaseString]] || [str_fields.lowercaseString containsString:[@"Author Of Original Book" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"container-title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"website name" lowercaseString]] || [str_fields.lowercaseString containsString:[@"Name of news source" lowercaseString]]) {
                NSString *str_ref_value = [dict_data objectForKey:@"collection-title"];
                [arr_ref_value addObject:str_ref_value];
            } else if ([str_fields.lowercaseString containsString:[@"date produced" lowercaseString]]) {
                NSDictionary *dict_issued = [dict_data objectForKey:@"accessed"];
                NSArray *arr_dateparts = [dict_issued objectForKey:@"date-parts"];
                NSArray *arr_dates = [arr_dateparts objectAtIndex:0];
                NSString *str_full_date = [NSString stringWithFormat:@"%@,%@,%@",[arr_dates objectAtIndex:0],[arr_dates objectAtIndex:1],[arr_dates objectAtIndex:2]];
                
                [arr_ref_value addObject:str_full_date];
            } else if ([str_fields.lowercaseString containsString:[@"format" lowercaseString]]) {
                [arr_ref_value addObject:[dict_data objectForKey:@"medium"]];
            } else if ([str_fields.lowercaseString containsString:[@"issue number" lowercaseString]]) {
                [arr_ref_value addObject:[dict_data objectForKey:@"issue"]];
            } else if ([str_fields.lowercaseString containsString:[@"Date Original Book Published" lowercaseString]]) {
                NSDictionary *dict_issued = [dict_data objectForKey:@"issued"];
                NSArray *arr_dateparts = [dict_issued objectForKey:@"date-parts"];
                NSArray *arr_dates = [arr_dateparts objectAtIndex:0];
                NSString *str_full_date = [NSString stringWithFormat:@"%@,%@,%@",[arr_dates objectAtIndex:0],[arr_dates objectAtIndex:1],[arr_dates objectAtIndex:2]];
                
                [arr_ref_value addObject:str_full_date];
            }
            
            else {
                NSString *str_ref_value = [dict_data objectForKey:[str_fields lowercaseString]];
                [arr_ref_value addObject:str_ref_value];
            }
        }
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[IndexViewController sharedIndexVC] hideTopMenu];
    self.navigationController.navigationBar.barTintColor = [self.view.tintColor colorWithAlphaComponent:0.7];
    self.navigationController.navigationBar.backgroundColor = [self.view.tintColor colorWithAlphaComponent:0.5];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(func_create_reference:) name:@"create_reference" object:nil];
}

-(void)func_create_reference:(NSNotification*)noti_selected_style_Project_ID {
    
//    NSDictionary *dict_selected = @{@"selected_style":str_selected_style,@"project_ID":selectedProject.name};
    
    NSDictionary *dict_selected = noti_selected_style_Project_ID.object;
    self.str_Selected_Style = [dict_selected objectForKey:@"selected_style"];
    self.str_Selected_Project_Tile = [dict_selected objectForKey:@"project_ID"];
    
    [self func_save_API];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
//    if ([[[typeDictionary objectForKey:@"fields"] objectAtIndex:0] isEqualToString:@"Author Double Field"])  {
        return 2;
//    }
//    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return arr_full_name.count;
    } else {
        return arr_ref.count;
    }
    
    
    
    // Return the number of rows in the section.
    
//    if ([[[typeDictionary objectForKey:@"fields"] objectAtIndex:0] isEqualToString:@"Author Double Field"]) {
//        if (section == 0)
//            return  [authorArray count];
//        return [[typeDictionary objectForKey:@"fields"] count] - 1;
//    }
//
//    if([[typeDictionary objectForKey:@"name"] isEqualToString:@"Website"]) {//additional textfield for Website type
//        return [[typeDictionary objectForKey:@"fields"] count] + 1;
//    }
//
//    return [[typeDictionary objectForKey:@"fields"] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [self func_DoubleTableViewCell:tableView indexPath:indexPath];
    } else {
//        if ([[self.title lowercaseString] isEqualToString:[@"book chapter" lowercaseString]]) {
//            if (indexPath.row == 1) {
//                return [self func_DoubleTableViewCell:tableView indexPath:indexPath];
//            } else {
//                return [self func_FullWidthTableViewCell:tableView indexPath:indexPath];
//            }
//        } else {
            return [self func_FullWidthTableViewCell:tableView indexPath:indexPath];
//        }
    }
}



//        cell.textField.keyboardType = UIKeyboardTypeDefault;
//        if ([cell.textField.placeholder isEqualToString:@"Year"])
//            cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//        if ([cell.textField.placeholder isEqualToString:@"URL"] || [cell.textField.placeholder isEqualToString:@"Image URL"] || [cell.textField.placeholder isEqualToString:@"Video URL"] || [cell.textField.placeholder isEqualToString:@"Website"])
//            cell.textField.keyboardType = UIKeyboardTypeURL;
//        cell.textField.text = [NSString stringWithFormat:@"%@",[refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row]]];
    
//{
//    if ([[[typeDictionary objectForKey:@"fields"] objectAtIndex:0] isEqualToString:@"Author Double Field"]) {
//        if (indexPath.section == 0) {
//            DoubleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"doubleCell" forIndexPath:indexPath];
//            cell.delegate = self;
//            //            cell.firstTextField.placeholder = [NSString stringWithFormat:@"Firstname"];
//            //            cell.firstTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Firstname"];
//            //            cell.secondTextField.placeholder = [NSString stringWithFormat:@"Lastname"];
//            //            cell.secondTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Lastname"];
//
//            cell.firstTextField.placeholder = [NSString stringWithFormat:@"Surname"];
//            cell.firstTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Surname"];
//            cell.secondTextField.placeholder = [NSString stringWithFormat:@"Initial(s)"];
//            cell.secondTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Initials"];
//
//            if (singleAuthor == TRUE)
//                [cell.addRemoveButton setHidden:TRUE];
//
//            [cell.addRemoveButton setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
//            [cell.addRemoveButton setFrame:CGRectMake(283, 14, 22, 22)];
//
//            if (indexPath.row != 0) {
//                cell.firstTextField.placeholder = [NSString stringWithFormat:@"Surname %i",indexPath.row + 1];
//                cell.firstTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Surname"];
//                cell.secondTextField.placeholder = [NSString stringWithFormat:@"Initial(s) %i",indexPath.row + 1];
//                cell.secondTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Initial(s)"];
//
//                [cell.addRemoveButton setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
//                [cell.addRemoveButton setFrame:CGRectMake(283, 14, 22, 22)];
//            }
//            return cell;
//        } else {
//            FullWidthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fullWidthCell" forIndexPath:indexPath];
//            //            cell.delegate = self;
//            cell.textField.placeholder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row + 1]];
//
//            cell.textField.keyboardType = UIKeyboardTypeDefault;
//
//            if ([cell.textField.placeholder isEqualToString:@"Year"])
//                cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//            if ([cell.textField.placeholder isEqualToString:@"URL"] || [cell.textField.placeholder isEqualToString:@"Image URL"] || [cell.textField.placeholder isEqualToString:@"Video URL"] || [cell.textField.placeholder isEqualToString:@"Website"])
//                cell.textField.keyboardType = UIKeyboardTypeURL;
//
//            NSString *str_Fields = [NSString stringWithFormat:@"%@",[refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row + 1]]];
//            NSLog(@"%@",str_Fields);
//
//            if ([str_Fields isEqual:@"(null)"]) {
//                cell.textField.text = @"";
//            } else {
//                cell.textField.text = str_Fields;
//            }
//
//            cell.textField.tag = indexPath.row + 1;
//            cell.textField.delegate = self;
//
//            //            [cell.textField removeTarget:self action:@selector(func_TextField:) forControlEvents:UIControlEventEditingDidBegin];
//            //            [cell.textField addTarget:self action:@selector(func_TextField:) forControlEvents:UIControlEventEditingDidBegin];
//
//            return cell;
//        }
//    } else {
//        NSString *typeName = [typeDictionary objectForKey:@"name"];
//        if([typeName isEqualToString:@"Website"]) {
//            if(indexPath.row == 0) {
//                SearchBarTableViewCell *result = [tableView dequeueReusableCellWithIdentifier:@"SearchBarTableViewCell" forIndexPath:indexPath];
//                result.delegate = self;
//
//                NSString *strVal = [refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row]];
//                if(strVal == nil) {
//                    result.searchField.text = @"";
//                } else {
//                    result.searchField.text = strVal;
//                }
//
//                //result.searchField.text = @"https://www.macworld.com/article/3137575/macs/the-new-macbook-pro-isnt-just-a-laptop-its-a-strategy-shift.html";
//                //result.searchField.text = @"https://www.macrumors.com/roundup/ipad/";
//
//                return result;
//            } else {
//                FullWidthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fullWidthCell" forIndexPath:indexPath];
//                cell.delegate = self;
//                cell.textField.placeholder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row-1]];
//
//                cell.textField.keyboardType = UIKeyboardTypeDefault;
//
//                if ([cell.textField.placeholder isEqualToString:@"Year"])
//                    cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//                if ([cell.textField.placeholder isEqualToString:@"URL"] || [cell.textField.placeholder isEqualToString:@"Image URL"] || [cell.textField.placeholder isEqualToString:@"Video URL"] || [cell.textField.placeholder isEqualToString:@"Website"])
//                    cell.textField.keyboardType = UIKeyboardTypeURL;
//
//                cell.textField.text = [NSString stringWithFormat:@"%@",[refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row-1]]];
//                return cell;
//            }
//        } else {
//            FullWidthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fullWidthCell" forIndexPath:indexPath];
//            cell.delegate = self;
//            cell.textField.placeholder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row]];
//
//            cell.textField.keyboardType = UIKeyboardTypeDefault;
//
//            if ([cell.textField.placeholder isEqualToString:@"Year"])
//                cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//            if ([cell.textField.placeholder isEqualToString:@"URL"] || [cell.textField.placeholder isEqualToString:@"Image URL"] || [cell.textField.placeholder isEqualToString:@"Video URL"] || [cell.textField.placeholder isEqualToString:@"Website"])
//                cell.textField.keyboardType = UIKeyboardTypeURL;
//
//            cell.textField.text = [NSString stringWithFormat:@"%@",[refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row]]];
//
//            return cell;
//        }
//    }
//}

-(FullWidthTableViewCell*)func_FullWidthTableViewCell:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath {
    FullWidthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fullWidthCell" forIndexPath:indexPath];
    
    //        cell.delegate = self;
    cell.textField.placeholder = [NSString stringWithFormat:@"%@",[arr_ref objectAtIndex:indexPath.row]];
    cell.textField.text = [NSString stringWithFormat:@"%@",[arr_ref_value objectAtIndex:indexPath.row]];
    
    cell.textField.delegate = self;
    
    return cell;
}

-(DoubleTableViewCell*) func_DoubleTableViewCell:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath {
    DoubleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"doubleCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    cell.firstTextField.placeholder = [NSString stringWithFormat:@"First Name(s)"];
    cell.secondTextField.placeholder = [NSString stringWithFormat:@"Last Name(s)"];
    
    cell.firstTextField.delegate = self;
    cell.secondTextField.delegate = self;
    
    NSString *str_fullname_value = [NSString stringWithFormat:@"%@",[arr_full_name_value objectAtIndex:indexPath.row]];
    
    if ([str_fullname_value isEqualToString:@"First Name-Last Name"]) {
        cell.firstTextField.text = @"";
        cell.secondTextField.text = @"";
    } else {
        NSArray *arr_fullname_value = [str_fullname_value componentsSeparatedByString:@"-"];
        if ([[arr_fullname_value objectAtIndex:0] isEqualToString:@"First Name"]) {
            cell.firstTextField.text = @"";
        } else {
            cell.firstTextField.text = [arr_fullname_value objectAtIndex:0];
        }
        
        if ([[arr_fullname_value objectAtIndex:0] isEqualToString:@"Last Name"]) {
            cell.secondTextField.text = @"";
        } else {
            cell.secondTextField.text = [arr_fullname_value objectAtIndex:1];
        }
    }
    
    if (indexPath.row == 0) {
        [cell.addRemoveButton setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
    } else {
        [cell.addRemoveButton setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
    }
    
    return cell;
}


-(void)addButtonPressedAtCell:(DoubleTableViewCell *)cell {
    [self.view endEditing:TRUE];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath.row == 0) {
        [arr_full_name addObject:@"First Name-Last Name"];
    } else {
        [arr_full_name removeObjectAtIndex:indexPath.row];
    }
    
    NSArray *arr_fullname_copy = [NSArray arrayWithArray:arr_full_name_value];
    NSLog(@"%@",arr_fullname_copy);
    
    for (int i=0 ; i<arr_full_name.count; i++) {
        if (arr_fullname_copy.count > i) {
            [arr_full_name_value replaceObjectAtIndex:i withObject:[arr_full_name_value objectAtIndex:i]];
        } else {
            [arr_full_name_value addObject:@"First Name-Last Name"];
        }
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:[indexPath section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)projectSelectorDidSelectProject:(Project *)project {
    _currentProject = project;
    [self saveButtonPressed:nil];
}

-(void) func_save_API {
    NSMutableDictionary *dic_JSON = [[NSMutableDictionary alloc]init];
    
    NSDictionary *dict_Type = [[JSON_Param sharedInstance] func_JSON_Type];
//    NSDictionary *dict_Type = [[JSON_Param sharedInstance] func_JSON_Type];
    
    [dic_JSON setObject:@"ITEM-1" forKey:@"id"];
    [dic_JSON setObject:[dict_Type objectForKey:[self.title lowercaseString]] forKey:@"type"];
    
    NSMutableArray *arr_Author = [[NSMutableArray alloc]init];
    for (int i=0; i<arr_full_name_value.count; i++) {
        NSString *str_fullname_value = [NSString stringWithFormat:@"%@",[arr_full_name_value objectAtIndex:i]];
        
        NSArray *arr_fullname_value = [str_fullname_value componentsSeparatedByString:@"-"];
        
        NSString *str_firstName = [arr_fullname_value objectAtIndex:0];
        NSString *str_lastname = [arr_fullname_value objectAtIndex:1];
        
        NSDictionary *dict_Author = @{@"family":str_firstName,@"given":str_lastname};
        [arr_Author addObject:dict_Author];
    }
    
    [dic_JSON setObject:arr_Author forKey:@"author"];
    
    NSArray *arr_JSON_Param = [[JSON_Param sharedInstance] func_JSON_Param];
    for (int i=0; i<arr_ref.count; i++) {
        NSString *str_ref = [arr_ref objectAtIndex:i];
        NSLog(@"%@",str_ref);
        
        for (int j=0; j<arr_JSON_Param.count; j++) {
            NSString *str_JSON_Param = [arr_JSON_Param objectAtIndex:j];
            
            if ([[str_ref lowercaseString] isEqualToString:[str_JSON_Param lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:[str_ref lowercaseString]];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Blog Title" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Publisher Place" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"publisher-place"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Page Range" lowercaseString]]) {
                if ([[self.title lowercaseString] isEqualToString:[@"book chapter" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"book review" lowercaseString]]) {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"number-of-volumes"];
                } else if ([[self.title lowercaseString] isEqualToString:[@"encyclopedia entry" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"journal article" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"magazine article" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"news article" lowercaseString]]  || [[self.title lowercaseString] isEqualToString:[@"report" lowercaseString]]) {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"page"];
                } else {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"number-of-pages"];
                }
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Book Author" lowercaseString]]) {
                NSString *str_book_author = [arr_ref_value objectAtIndex:i];
                NSArray *arr_book_author = [str_book_author componentsSeparatedByString:@" "];
                
                NSString *str_firstName = [arr_book_author objectAtIndex:0];
                NSString *str_lastname = @"";
                
                if ([arr_book_author count] > 1) {
                    for (int i =0 ; i<arr_book_author.count; i++) {
                        if (i > 0) {
                            if ([str_lastname isEqualToString:@""]) {
                                 str_lastname = [NSString stringWithFormat:@"%@",[arr_book_author objectAtIndex:i]];
                            } else {
                                str_lastname = [NSString stringWithFormat:@"%@ %@",str_lastname,[arr_book_author objectAtIndex:i]];
                            }
                        }
                    }
                }
                
                NSDictionary *dict_Author = @{@"family":str_firstName,@"given":str_lastname};
                arr_book_author = @[dict_Author];
                
                [dic_JSON setObject:arr_book_author forKey:@"collection-editor"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Name Of Journal Proceedings" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Title of Article" lowercaseString]] || [[str_ref lowercaseString] isEqualToString:[@"Title of original book" lowercaseString]]) {
                if ([[self.title lowercaseString] isEqualToString:[@"journal article" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"magazine article" lowercaseString]] || [[self.title lowercaseString] isEqualToString:[@"news article" lowercaseString]]) {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"title"];
                } else {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
                }
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Database Name" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"source"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"NAME OF DICTIONARY" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Name Of Encyclopedia" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"distributor" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"publisher"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"container" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Date Of Interview" lowercaseString]]) {
                NSArray *arr_DateAccessed = [[arr_ref_value objectAtIndex:i] componentsSeparatedByString:@","];
                NSArray *arr_DateParts = @[arr_DateAccessed];
                NSDictionary *dict_Dateparts = @{@"date-parts":arr_DateParts};
                [dic_JSON setObject:dict_Dateparts forKey:@"issued"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"interviewer" lowercaseString]]) {
                NSString *str_book_author = [arr_ref_value objectAtIndex:i];
                NSArray *arr_book_author = [str_book_author componentsSeparatedByString:@" "];
                
                NSString *str_firstName = [arr_book_author objectAtIndex:0];
                NSString *str_lastname = @"";
                
                if ([arr_book_author count] > 1) {
                    for (int i =0 ; i<arr_book_author.count; i++) {
                        if (i > 0) {
                            if ([str_lastname isEqualToString:@""]) {
                                str_lastname = [NSString stringWithFormat:@"%@",[arr_book_author objectAtIndex:i]];
                            } else {
                                str_lastname = [NSString stringWithFormat:@"%@ %@",str_lastname,[arr_book_author objectAtIndex:i]];
                            }
                        }
                    }
                }
                
                NSDictionary *dict_Author = @{@"family":str_firstName,@"given":str_lastname};
                arr_book_author = @[dict_Author];
                
                [dic_JSON setObject:arr_book_author forKey:@"interviewer"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"name of journal" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"volume number" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"volume"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"issue number" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"issue"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"doi" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"doi"];
            } else if ([[str_ref lowercaseString] containsString:[@"date issued" lowercaseString]]) {
                NSArray *arr_DateAccessed = [[arr_ref_value objectAtIndex:i] componentsSeparatedByString:@","];
                NSArray *arr_DateParts = @[arr_DateAccessed];
                NSDictionary *dict_Dateparts = @{@"date-parts":arr_DateParts};
                [dic_JSON setObject:dict_Dateparts forKey:@"issued"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"case name" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"title"];
            } else if ([[str_ref lowercaseString] containsString:[@"date of decision" lowercaseString]]) {
                NSArray *arr_DateAccessed = [[arr_ref_value objectAtIndex:i] componentsSeparatedByString:@","];
                NSArray *arr_DateParts = @[arr_DateAccessed];
                NSDictionary *dict_Dateparts = @{@"date-parts":arr_DateParts};
                [dic_JSON setObject:dict_Dateparts forKey:@"issued"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"NAME OF MAGAZINE" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"name of news source" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"website name" lowercaseString]]) {
                if ([self.title isEqualToString:@"website"]) {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
                } else {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"collection-title"];
                }
            } else if ([[str_ref lowercaseString] containsString:[@"date produced" lowercaseString]]) {
                NSArray *arr_DateAccessed = [[arr_ref_value objectAtIndex:i] componentsSeparatedByString:@","];
                NSArray *arr_DateParts = @[arr_DateAccessed];
                NSDictionary *dict_Dateparts = @{@"date-parts":arr_DateParts};
                [dic_JSON setObject:dict_Dateparts forKey:@"accessed"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"format" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"medium"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"author of original book" lowercaseString]]) {
                NSString *str_book_author = [arr_ref_value objectAtIndex:i];
                NSArray *arr_book_author = [str_book_author componentsSeparatedByString:@" "];
                
                NSString *str_firstName = [arr_book_author objectAtIndex:0];
                NSString *str_lastname = @"";
                
                if ([arr_book_author count] > 1) {
                    for (int i =0 ; i<arr_book_author.count; i++) {
                        if (i > 0) {
                            if ([str_lastname isEqualToString:@""]) {
                                str_lastname = [NSString stringWithFormat:@"%@",[arr_book_author objectAtIndex:i]];
                            } else {
                                str_lastname = [NSString stringWithFormat:@"%@ %@",str_lastname,[arr_book_author objectAtIndex:i]];
                            }
                        }
                    }
                }
                
                NSDictionary *dict_Author = @{@"family":str_firstName,@"given":str_lastname};
                arr_book_author = @[dict_Author];
                
                [dic_JSON setObject:arr_book_author forKey:@"container-author"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"title of original book" lowercaseString]]) {
                [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"container-title"];
            } else if ([[str_ref lowercaseString] isEqualToString:[@"Author of original book" lowercaseString]]) {
                NSString *str_book_author = [arr_ref_value objectAtIndex:i];
                NSArray *arr_book_author = [str_book_author componentsSeparatedByString:@" "];
                
                NSString *str_firstName = [arr_book_author objectAtIndex:0];
                NSString *str_lastname = @"";
                
                if ([arr_book_author count] > 1) {
                    for (int i =0 ; i<arr_book_author.count; i++) {
                        if (i > 0) {
                            if ([str_lastname isEqualToString:@""]) {
                                str_lastname = [NSString stringWithFormat:@"%@",[arr_book_author objectAtIndex:i]];
                            } else {
                                str_lastname = [NSString stringWithFormat:@"%@ %@",str_lastname,[arr_book_author objectAtIndex:i]];
                            }
                        }
                    }
                }
                
                NSDictionary *dict_Author = @{@"family":str_firstName,@"given":str_lastname};
                arr_book_author = @[dict_Author];
                
                [dic_JSON setObject:arr_book_author forKey:@"container-author"];
            } else {
                if ([[str_ref lowercaseString] containsString:[@"Published" lowercaseString]]) {
                    NSArray *arr_DateAccessed = [[arr_ref_value objectAtIndex:i] componentsSeparatedByString:@","];
                    NSArray *arr_DateParts = @[arr_DateAccessed];
                    NSDictionary *dict_Dateparts = @{@"date-parts":arr_DateParts};
                    [dic_JSON setObject:dict_Dateparts forKey:@"issued"];
                } else if ([[str_ref lowercaseString] containsString:[@"Accessed" lowercaseString]]) {
                    NSArray *arr_DateAccessed = [[arr_ref_value objectAtIndex:i] componentsSeparatedByString:@","];
                    NSArray *arr_DateParts = @[arr_DateAccessed];
                    NSDictionary *dict_Dateparts = @{@"date-parts":arr_DateParts};
                    [dic_JSON setObject:dict_Dateparts forKey:@"accessed"];
                } else if ([[str_ref lowercaseString] containsString:[@"title" lowercaseString]]) {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"title"];
                } else if ([[str_ref lowercaseString] isEqualToString:[@"Annotation" lowercaseString]]) {
                    [dic_JSON setObject:[arr_ref_value objectAtIndex:i] forKey:@"note"];
                }
            }
        }
    }
    
    NSArray *arr_JSON = [NSArray arrayWithObject:dic_JSON];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr_JSON options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self func_POST_API:jsonString];
}

- (IBAction)saveButtonPressed:(UIButton*)sender {
    [self.view endEditing:TRUE];
    
    NSArray *arr_all_key_JSON_Type = [[[JSON_Param sharedInstance] func_JSON_Type] allKeys];
    NSString *str_valid = @"0";
    for (NSString *str_title in arr_all_key_JSON_Type) {
        if ([[self.title lowercaseString] isEqualToString:[str_title lowercaseString]]) {
            str_valid = @"1";
            break;
        } else {
            str_valid = @"0";
        }
    }
    
    if ([str_valid isEqualToString:@"0"]) {
        [[IndexViewController sharedIndexVC] error_POPUP:@"is under construction"];
        return;
    }
    
    bool is_Valid = NO;
    
    for (int i=0; i<arr_full_name_value.count; i++) {
        NSString *str_fullname_value = [NSString stringWithFormat:@"%@",[arr_full_name_value objectAtIndex:i]];
        NSArray *arr_fullname_value = [str_fullname_value componentsSeparatedByString:@"-"];
        
        if ([[[arr_fullname_value objectAtIndex:0] lowercaseString] containsString:[@"First Name" lowercaseString]]) {
            [self func_alert:@"Enter First Name" status:@"Error"];
            is_Valid = NO;
            break;
        } else {
            is_Valid = YES;
        }
        
        if ([[[arr_fullname_value objectAtIndex:1] lowercaseString] containsString:[@"Last Name" lowercaseString]]) {
            [self func_alert:@"Enter Last Name" status:@"Error"];
            is_Valid = NO;
            break;
        } else {
            is_Valid = YES;
        }
    }
    
    if (!is_Valid) {
        return;
    }
    
    for (int i=0; i<arr_ref_value.count; i++) {
        NSString *str_ref_value = [NSString stringWithFormat:@"%@",[arr_ref_value objectAtIndex:i]];
        if ([str_ref_value isEqualToString:@""]) {
            NSString *str_message = [NSString stringWithFormat:@"Enter %@",[arr_ref objectAtIndex:i]];
            [self func_alert:str_message status:@"Error"];
            is_Valid = NO;
            break;
        } else {
            is_Valid = YES;
        }
    }
    
    if (!is_Valid) {
        return;
    }
    
    if ([[[JSON_Param sharedInstance] str_selected_tabbar] isEqualToString:@"0"]) {
        ProjectsTableViewController *project_Table_VC = [self.storyboard instantiateViewControllerWithIdentifier:@"projectsVC"];
        [self.navigationController pushViewController:project_Table_VC animated:YES];
    } else {
        [self func_save_API];
    }
    
}

-(void)func_Back {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[IndexViewController sharedIndexVC] saveSucessfull];
        [self.navigationController popViewControllerAnimated:YES];
    });
}

-(void) func_Save_Local_DB_result_API:(NSString*)result_API str_json:(NSArray*)arr_JSON_For_Local_DB {
//    if (_currentProject == nil) {
//        ProjectSelectorViewController *selector = [self.storyboard instantiateViewControllerWithIdentifier:@"projectSelectorVC"];
//        selector.delegate = self;
//        [self presentViewController:selector animated:true completion:nil];
//        return;
//    }
    
//    if (_currentReference != nil) {
//        if ([self hasEmptyCells])
//            return;

//        if ([_currentProject.onlineProject boolValue] == TRUE)
//        {
//            loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
//            [self.view.window addSubview:loadingView];
//            [loadingView fadeIn];
//
//            NSError *error;
//
//            if ([refDataDictionary objectForKey:@"Author Double Field"] != nil)
//                [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"]) {
//                NSMutableDictionary *refData = [NSJSONSerialization JSONObjectWithData:[_currentReference.data dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
//                [refDataDictionary setObject:[refData objectForKey:@"hasEditors"]  forKey:@"hasEditors"];
//            }
//            [refDataDictionary setObject:result_API  forKey:@"result_API"];
//
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//            dateFormatter.dateFormat = @"yyyyMMDDhhmmss";
//            _currentReference.projectID = [dateFormatter stringFromDate:[NSDate date]];
//
//            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
//
//            [[ReferencingApp sharedInstance] editReferenceWithData:dataString referenceType:_currentReference.referenceType projectID:_currentReference.projectID andReferenceID:_currentReference.referenceID];
//            [self func_Back];
//        } else {
            NSError *error;
//            if ([refDataDictionary objectForKey:@"Author Double Field"] != nil)
//                [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
//            {
//                NSMutableDictionary *refData = [NSJSONSerialization JSONObjectWithData:[_currentReference.data dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
//                [refDataDictionary setObject:[refData objectForKey:@"hasEditors"]  forKey:@"hasEditors"];
//            }
    
            [refDataDictionary setObject:arr_JSON_For_Local_DB forKey:@"data"];
            [refDataDictionary setObject:result_API  forKey:@"result_API"];
    
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//            dateFormatter.dateFormat = @"yyyyMMDDhhmmss";
//            _currentReference.projectID = [dateFormatter stringFromDate:[NSDate date]];
    
            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
    
//            _currentReference.data = dataString;
    
//            [[ReferencingApp sharedInstance] saveDatabase];
//
//            NSArray *projectsArray = [[ReferencingApp sharedInstance]  getDataBaseWithName:@"Project"];
//            NSLog(@"%@",projectsArray);
    
            NSMutableDictionary *dict_save_local_DB = [[NSMutableDictionary alloc]init];
            
            [dict_save_local_DB setValue:self.str_Selected_Project_Tile forKey:@"projectID"];
            [dict_save_local_DB setValue:dataString forKey:@"data"];
            [dict_save_local_DB setValue:self.title forKey:@"referenceType"];
    
            if ([self.is_Edit isEqualToString:@"1"]) {
                [dict_save_local_DB setValue:str_referenceID forKey:@"referenceID"];
                [[ReferencingApp sharedInstance] update_Datebase:dict_save_local_DB];
            } else {
                [[ReferencingApp sharedInstance] save_Local_Database:dict_save_local_DB];
            }
            [self func_Back];
    
//            [[IndexViewController sharedIndexVC] saveSucessfull];

            /*********
             *********
             *********
             *********
             *********
             *********/
            //            [self.navigationController popViewControllerAnimated:true];
//        }
//    }
//    else
//    {
//        if ([self hasEmptyCells])
//            return;
//
//        if ([_currentProject.onlineProject boolValue] == TRUE)
//        {
//            NSError *error;
//            [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
//                [refDataDictionary setObject:[NSNumber numberWithBool:hasEditors] forKey:@"hasEditors"];
//            [refDataDictionary setObject:result_API  forKey:@"result_API"];
//            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
//
//            loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
//            [self.view.window addSubview:loadingView];
//            [loadingView fadeIn];
//
//            [[ReferencingApp sharedInstance] createReferenceWithData:dataString referenceType:[typeDictionary objectForKey:@"name"] andProjectID:_currentProject.projectID];
//            [self func_Back];
//        } else {
//            AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//            NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
//            Reference *newRef = [NSEntityDescription insertNewObjectForEntityForName:@"Reference" inManagedObjectContext:managedObjectContext];
//
//            NSError *error;
//            [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
//                [refDataDictionary setObject:[NSNumber numberWithBool:hasEditors] forKey:@"hasEditors"];
//            [refDataDictionary setObject:result_API  forKey:@"result_API"];
//            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
//
//            newRef.dateCreated = [NSDate date];
//            newRef.data = dataString;
//            newRef.referenceType = [typeDictionary objectForKey:@"name"];
//
//            _currentReference = newRef;
//
//            [_currentProject addReferencesObject:newRef];
//
//            [[ReferencingApp sharedInstance] saveDatabase];
//
//            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//            for (UIViewController *aViewController in allViewControllers) {
//                if ([aViewController isKindOfClass:[ProjectReferencesTableViewController class]]) {
//                    //                    [[IndexViewController sharedIndexVC] saveSucessfull];
//                    //                    [self.navigationController popToViewController:aViewController animated:TRUE];
//                    //                    return;
//                    break;
//                }
//            }
//
//            [self func_Back];
//            //            [[IndexViewController sharedIndexVC] saveSucessfull];
//            /*********
//             *********
//             *********
//             *********
//             *********
//             *********/
//            //            [self.navigationController popViewControllerAnimated:true];
//        }
//
//    }
}

-(void)unloadLoadingView {
    [UIView animateWithDuration:0.5f animations:^{
        loadingView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [loadingView removeFromSuperview];
        loadingView = nil;
        
    }];
}

//#pragma mark REFERECING APP DELEGATE
//-(void)referenceEditingFailedWithErrror:(NSString *)errorString {
//    [self.tableView reloadData];
//
//    [self unloadLoadingView];
//
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                   message: errorString
//                                                  delegate: self
//                                         cancelButtonTitle:@"OK"
//                                         otherButtonTitles:nil];
//    [alert show];
//}
//
//-(void)referenceCreationFailedWithEror:(NSString *)errorString
//{
//    [self unloadLoadingView];
//
//
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                   message: errorString
//                                                  delegate: self
//                                         cancelButtonTitle:@"OK"
//                                         otherButtonTitles:nil];
//    [alert show];
//}
//
//
//-(void)referenceSucessfullyEdited:(NSDictionary *)dictionary
//{
//
//    _currentReference.data = [dictionary objectForKey:@"data"];
//
//    [self unloadLoadingView];
//
//    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//    for (UIViewController *aViewController in allViewControllers) {
//        if ([aViewController isKindOfClass:[ProjectReferencesTableViewController class]]) {
//            [[IndexViewController sharedIndexVC] saveSucessfull];
//
//            [self.navigationController popToViewController:aViewController animated:TRUE];
//            return;
//
//        }
//    }
//    [[IndexViewController sharedIndexVC] saveSucessfull];
//
//    [self.navigationController popViewControllerAnimated:true];
//}
//
//-(void)referenceSucessfullyCreated:(NSDictionary *)dictionary
//{
//    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//    NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
//
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Reference" inManagedObjectContext:managedObjectContext];
//    Reference *newRef = [[Reference alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
//
//    newRef.projectID = [[dictionary objectForKey:@"project_id"] stringValue];
//    newRef.referenceType = [dictionary objectForKey:@"reference_type"];
//    newRef.data = [dictionary objectForKey:@"data"];
//    newRef.referenceID = [[dictionary objectForKey:@"id"] stringValue];
//
//    NSString *dateString = [dictionary objectForKey:@"created_at"];
//    dateString = [dateString substringToIndex:[dateString length]-5];
//
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-M-d'T'H:m:s"];
//
//    newRef.dateCreated = [dateFormat dateFromString:dateString];
//
//    _currentReference = newRef;
//
//    [_currentProject addReferencesObject:newRef];
//
//    [self unloadLoadingView];
//
//    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//    for (UIViewController *aViewController in allViewControllers) {
//        if ([aViewController isKindOfClass:[ProjectReferencesTableViewController class]]) {
//            [[IndexViewController sharedIndexVC] saveSucessfull];
//
//            [self.navigationController popToViewController:aViewController animated:TRUE];
//            return;
//        }
//    }
//    [[IndexViewController sharedIndexVC] saveSucessfull];
//    [self.navigationController popViewControllerAnimated:true];
//}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    CGPoint point = [textField.superview convertPoint:textField.frame.origin toView:self.tableView];
    NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint:point];
    NSLog(@"\nsection %i\nrow%i", indexPath.section, indexPath.row);
    
    if ([[textField.placeholder lowercaseString] containsString:@"date"]) {
        [arr_ref_value replaceObjectAtIndex:indexPath.row withObject:str_selecte_date];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        if (indexPath.section == 0) {
            DoubleTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            
            if (textField == cell.firstTextField) {
                NSString *str_full_name;
                if ([cell.secondTextField.text isEqualToString:@""]) {
                    str_full_name = [NSString stringWithFormat:@"%@-%@",textField.text,cell.secondTextField.placeholder];
                } else {
                    str_full_name = [NSString stringWithFormat:@"%@-%@",textField.text,cell.secondTextField.text];
                }
                [arr_full_name_value replaceObjectAtIndex:indexPath.row withObject:str_full_name];
            } else {
                NSString *str_full_name;
                if ([cell.firstTextField.text isEqualToString:@""]) {
                    str_full_name = [NSString stringWithFormat:@"%@-%@",cell.firstTextField.placeholder,textField.text];
                } else {
                    str_full_name = [NSString stringWithFormat:@"%@-%@",cell.firstTextField.text,textField.text];
                }
                [arr_full_name_value replaceObjectAtIndex:indexPath.row withObject:str_full_name];
            }
        } else {
            [arr_ref_value replaceObjectAtIndex:indexPath.row withObject:textField.text];
        }
    }
    
    str_selecte_date = @"";
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([[textField.placeholder lowercaseString] containsString:@"date"]) {
        UIDatePicker *date_picker ;
        date_picker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        [date_picker setDatePickerMode:UIDatePickerModeDate];
        [date_picker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        date_picker.tag = 0;
        textField.inputView = date_picker;
        
        UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setTintColor:[UIColor grayColor]];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btn_done_ToolBar)];
        UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
        
        [textField setInputAccessoryView:toolBar];
    } else {
        
    }
}

-(void)btn_done_ToolBar {
    [self.view endEditing:YES];
    
    if ([str_selecte_date isEqualToString:@""]) {
        NSDateFormatter * formatter =  [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy,MM,dd"];
        NSString *dateString = [formatter stringFromDate:[NSDate date]];
        
        str_selecte_date = dateString;
    }
}

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker {
    NSDateFormatter * formatter =  [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy,MM,dd"];
    NSString *dateString = [formatter stringFromDate:datePicker.date];
    
    str_selecte_date = dateString;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    textField.text = [textField.text capitalizedString];
    return true;
}

#pragma - SearchBarTableViewCellDelegate

- (NSString *) removeToken:(NSString *) strToken from:(NSString *) strString
{
    strString = [strString substringFromIndex:strToken.length];
    
    if([strString containsString:@"."]){
        NSRange range = [strString rangeOfString:@"."];
        if(range.location != NSNotFound){
            strString = [strString substringToIndex:range.location];
        }
    }
    
    return strString;
}



#pragma mark - WebDataManagerDelegate

//-(void)didExtractedDataFromURL:(WebDataManager *)dataManager dictionary:(NSDictionary *) responseDictionary
//{
//    if(responseDictionary != nil) {
//
//        NSDictionary *webData = responseDictionary;
//        NSString *strTitle = [webData objectForKey:@"title"];
//        NSString *strURL = [webData objectForKey:@"url"];
//        NSString *strAuthor = [webData objectForKey:@"author"];
//        NSString *strDate = [webData objectForKey:@"date"];
//
//        strAuthor = [self validateAuthor:strAuthor strURL:strURL];
//
//        NSString *lastAccessed = [Utilities dateToString:[NSDate date] format:@"dd MMMM YYYY"];
//        NSArray* fields = [typeDictionary objectForKey:@"fields"];
//        for(int i = 0; i < fields.count; ++i){
//            NSString *strKey = [fields objectAtIndex:i];
//            if([strKey isEqualToString:@"Title"]) {
//                [refDataDictionary setObject:strTitle forKey:strKey];
//            }
//            else if([strKey isEqualToString:@"URL"]){
//                [refDataDictionary setObject:strURL forKey:strKey];
//            }
//            else if([strKey isEqualToString:@"Last Accessed"]){
//                [refDataDictionary setObject:lastAccessed forKey:strKey];
//            }
//            else if([strKey isEqualToString:@"Year"]){
//                if(strDate != nil) {
//                    [refDataDictionary setObject:strDate forKey:strKey];
//                }
//            }
//            else if([strKey isEqualToString:@"Name of Website/Author"]){
//                if(strAuthor != nil) {
//                    [refDataDictionary setObject:strAuthor forKey:strKey];
//                }
//            }
//
//        }
//
//        [refDataDictionary setObject:strURL forKey:@"Web URL"];
//
//        [self.tableView reloadData];
//        [self unloadLoadingView];
//    } else {
//        [self unloadLoadingView];
//    }
//}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _arr_Style_Picker.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.arr_Style_Picker[row] uppercaseString];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"%@",self.arr_Style_Picker[row]);
    self.txt_ChooseYourCitationStyle.text = self.arr_Style_Picker[row];
}



-(void) func_get_styles {
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    NSString *full_URL = [NSString stringWithFormat:@"http://3.19.62.211:5000/get_styles"];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:full_URL]];
    
    //    NSString *userUpdate =[NSString stringWithFormat:@"user=%@&password=%@&api_id=%@&to=%@&text=%@",_username,_password,_apiID,[_numbers componentsJoinedByString:@","],_txtMsg.text, nil];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    //Convert the String to Data
    //    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    //    [urlRequest setHTTPBody:data1];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        //        NSLog(@"httpResponse is:- %ld",(long)httpResponse.statusCode);
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse.statusCode == 200) {
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            NSInteger success = [[responseDictionary objectForKey:@"status"] integerValue];
            if(success == 1) {
                NSArray *arrStyles = [responseDictionary objectForKey:@"styles"];
                for (NSString *strStyle in arrStyles) {
                    NSMutableString *strStyle_Filter = [[NSMutableString alloc]init];
                    strStyle_Filter = [[strStyle stringByReplacingOccurrencesOfString:@"/home/ubuntu/.local/lib/python2.7/site-packages/citeproc/data/styles/dependent/" withString:@""] mutableCopy];
                    strStyle_Filter = [[strStyle_Filter stringByReplacingOccurrencesOfString:@".csl" withString:@""] mutableCopy];
                    [self.arr_Style_Picker addObject:strStyle_Filter];
                }
            }
        }
    }];
    [dataTask resume];
}



-(void)func_get: (NSString*)json {
    //    NSString *str_Param = [NSString stringWithFormat:@"json:%@&style_name:%@",json,self.txt_ChooseYourCitationStyle.text];
    //    NSString *param = [NSString stringWithFormat:@"json=%@&style_name=%@",json,self.txt_ChooseYourCitationStyle.text];
    //    NSLog(@"str_Param \n%@",param);
    //    NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://3.19.62.211:5000/get"]];
    
    NSString *userUpdate =[NSString stringWithFormat:@"json=json&style_name=future-virology"];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    //Convert the String to Data
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [urlRequest setHTTPBody:data1];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse.statusCode == 200)
        {
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            NSLog(@"The response is - %@",responseDictionary);
            NSInteger success = [[responseDictionary objectForKey:@"success"] integerValue];
            if(success == 1)
            {
                NSLog(@"Login SUCCESS");
            }
            else
            {
                NSLog(@"Login FAILURE");
            }
        }
        else
        {
            NSLog(@"Error");
        }
    }];
    
    return;
}



//- (void) func_get:(NSString*)json {
////    NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
////                               @"User-Agent": @"PostmanRuntime/7.17.1",
////                               @"Accept": @"*/*",
////                               @"Cache-Control": @"no-cache",
////                               @"Postman-Token": @"bf5d93a5-0f94-4703-8fd1-dc78e54af53e,a155e00c-df58-4e5d-9af9-2832ca75a698",
////                               @"Host": @"3.19.62.211:5000",
////                               @"Content-Type": @"multipart/form-data; boundary=--------------------------053104914594977153577004",
////                               @"Accept-Encoding": @"gzip, deflate",
////                               @"Content-Length": @"666",
////                               @"Connection": @"keep-alive",
////                               @"cache-control": @"no-cache" };
//
//    NSDictionary *dict_Param = [[NSDictionary alloc] initWithObjectsAndKeys: json, @"json",
//                                self.txt_ChooseYourCitationStyle.text, @"style_name",
//                                nil];
//    NSLog(@"dict_Param is- %@",dict_Param);
//
////    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict_Param options:0 error:nil];
////    NSString *full_URL = [NSString stringWithFormat:@"http://3.19.62.211:5000/get"];
//
//    NSString * strServiceURL = [NSString stringWithFormat:@"http://3.19.62.211:5000/get"];
//
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
//
//    [manager POST:strServiceURL parameters:dict_Param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
////        if (fileData!=nil){
////            [formData appendPartWithFileData:fileData name:@"Key name on which you will upload file" fileName:@"Image name" mimeType:@"image/jpeg"];
////        }
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"Success: %@", responseObject);
////        block(responseObject,nil);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
////        block (nil , error);
//    }];
//}



//- (void) func_get:(NSString*)json {
//    NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
//                               @"User-Agent": @"PostmanRuntime/7.17.1",
//                               @"Accept": @"*/*",
//                               @"Cache-Control": @"no-cache",
//                               @"Postman-Token": @"bf5d93a5-0f94-4703-8fd1-dc78e54af53e,a155e00c-df58-4e5d-9af9-2832ca75a698",
//                               @"Host": @"3.19.62.211:5000",
//                               @"Content-Type": @"multipart/form-data; boundary=--------------------------053104914594977153577004",
//                               @"Accept-Encoding": @"gzip, deflate",
//                               @"Content-Length": @"666",
//                               @"Connection": @"keep-alive",
//                               @"cache-control": @"no-cache" };
//
//
//
////    NSArray *parameters = @[ @{ @"name": @"json", @"value": @"[
////                                {
////                                    \"author\" : [
////                                    {
////                                        \"given\" : \"iOS\",
////                                        \"family\" : \"Raja\"
////                                    }
////                                    ],
////                                    \"issued\" : {
////                                    \"date-parts\" : [
////                                    \"2018 10 1\"
////                                    ]
////                                },
////                                \"id\" : \"ITEM-1\",
////                                \"title\" : \"Post\",
////                                \"container-title\" : \"Blog\",
////                                \"type\" : \"post-weblog\",
////                                \"accessed\" : {
////                                \"date-parts\" : [
////                                \"2018 10 1\"
////                                ]
////                                },
////                             \"URL\" : \"Www.App.Com\"
////                             }
////                             ]" },
////    @{ @"name": @"style_name", @"value": @"future-virology" }];
//
//
//
//    NSString *boundary = @"----WebKitFormBoundary7MA4YWxkTrZu0gW";
//
//    NSDictionary *dict_Param = [[NSDictionary alloc] initWithObjectsAndKeys: json, @"json",
//                                self.txt_ChooseYourCitationStyle.text, @"style_name",
//                                nil];
//    NSLog(@"dict_Param is- %@",dict_Param);
//
//    NSError *error;
//    NSMutableString *body = [NSMutableString new];
//    for (NSDictionary *param in dict_Param) {
//        [body appendFormat:@"--%@\r\n", boundary];
//        if (param[@"fileName"]) {
//            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"; filename=\"%@\"\r\n", param[@"name"], param[@"fileName"]];
//            [body appendFormat:@"Content-Type: %@\r\n\r\n", param[@"contentType"]];
//            [body appendFormat:@"%@", [NSString stringWithContentsOfFile:param[@"fileName"] encoding:NSUTF8StringEncoding error:&error]];
//            if (error) {
//                NSLog(@"%@", error);
//            }
//        } else {
//            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"\r\n\r\n", param[@"name"]];
//            [body appendFormat:@"%@", param[@"value"]];
//        }
//    }
//    [body appendFormat:@"\r\n--%@--\r\n", boundary];
//
//
//    NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://3.19.62.211:5000/get"]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:10.0];
//    [request setHTTPMethod:@"POST"];
//    [request setAllHTTPHeaderFields:headers];
//    [request setHTTPBody:postData];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                        NSLog(@"%@", httpResponse);
//                                                    }
//                                                }];
//    [dataTask resume];
//}

-(void) func_POST_API:(NSString*)json {
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    NSDictionary *dict_Param = [[NSDictionary alloc] initWithObjectsAndKeys:
                                json, @"json",
                                self.str_Selected_Style, @"style",
                                nil];
    NSLog(@"dict_Param is- %@",dict_Param);
    NSString *boundary = [self generateBoundaryString];
    
//    NSArray *arr_JSON = [NSArray arrayWithObject:dic_JSON];
    
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSArray *arr_JSON_For_Local_DB = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    // configure the request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://ondemandcab.com/appentus_food/api/csl_v1/get"]];
    [request setHTTPMethod:@"POST"];
    // set content type
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:dict_Param paths:nil fieldName:nil];
    NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
    NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse.statusCode == 200) {
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            NSInteger success = [[responseDictionary objectForKey:@"status"] integerValue];
            if(success == 1) {
                NSString *str_result = [responseDictionary objectForKey:@"result"];
                NSLog(@"result is:- %@",str_result);
                
                NSString *str_MSG = [NSString stringWithFormat:@"%@",str_result];
                NSLog(@"%@",str_MSG);
//                NSLog(@"%@",[self convertHTML:str_MSG]);
                [self func_Save_Local_DB_result_API:str_result str_json:arr_JSON_For_Local_DB];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self func_alert:str_MSG status:@"Success"];
                });
            } else {
                NSString *str_error = [responseDictionary objectForKey:@"error"];
                NSLog(@"str_error is:- %@",str_error);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[IndexViewController sharedIndexVC] error_POPUP:str_error];
                });
            }
        }
    }];
    [task resume];
}



- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldName:(NSString *)fieldName {
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSString *)mimeTypeForPath:(NSString *)path {
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

-(NSAttributedString *)convertHTML:(NSString *)html {
    return [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]
                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                 documentAttributes:nil error:nil];
}



-(void) func_alert:(NSString*)str_message status:(NSString*)status {
    NSString *str_status = [NSString stringWithFormat:@"%@!",status];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:str_status message:str_message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}



@end

















































////
////  CreateReference_ViewController.m
////  Harvard
////
////  Created by appentus technologies pvt. ltd. on 10/14/19.
////  Copyright © 2019 SquaredDimesions. All rights reserved.
//
//
//
//#import "CreateReference_ViewController.h"
//
//#import "SegmentTableViewCell.h"
//#import "AppDelegate.h"
//#import "ProjectReferencesTableViewController.h"
//#import <ActionSheetPicker-3.0/ActionSheetPicker.h>
//
//#import "WebDataManager.h"
//#import "Utilities.h"
//#import "Picker_VC.h"
//#import "Date_Picker_ViewController.h"
////#import <AFNetworking/AFNetworking.h>
//#import <MBProgressHUD/MBProgressHUD.h>
//
//
//@import MobileCoreServices;    // only needed in iOS
//
//
//@interface CreateReference_ViewController () <WebDataManagerDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>
//
//@end
//
//@implementation CreateReference_ViewController
//
//- (void)viewDidDisappear:(BOOL)animated {
////    [self.tabBarController.tabBar setHidden:NO];
//}
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//
//    self.arr_Style_Picker = [[NSMutableArray alloc]init];
////    [self func_get_styles];
//
//    UIPickerView *objPickerView = [UIPickerView new];
//
//    objPickerView.delegate = self;
//    objPickerView.dataSource = self;
//
//    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
//    [toolBar setTintColor:[UIColor grayColor]];
//    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(func_Done_PickerView)];
//    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
//
//    [self.txt_ChooseYourCitationStyle setInputAccessoryView:toolBar];
//
//    self.txt_ChooseYourCitationStyle.inputView = objPickerView;
//
//    typeDictionary = [[NSMutableDictionary alloc] init];
//
//    // Uncomment the following line to preserve selection between presentations.
//    // self.clearsSelectionOnViewWillAppear = NO;
//
//    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//
//    self.tableView.delegate = self;
//    self.tableView.dataSource = self;
//
//    self.tbl_creat_reference.delegate = self;
//    self.tbl_creat_reference.dataSource = self;
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(picker_Recieve:) name:@"picker_Recieve"  object:nil];
//
//    [ReferencingApp sharedInstance].delegate = self;
//
//    if ([self.title isEqualToString:@"Blog"] ||
//        [self.title isEqualToString:@"Dissertation"] ||
//        // [self.title isEqualToString:@"Encyclopedia"] ||
//        [self.title isEqualToString:@"Map"] ||
//        [self.title isEqualToString:@"Podcast"] ||
//        [self.title isEqualToString:@"Powerpoint"] ||
//        [self.title isEqualToString:@"Newspaper"]
//        ) {
//        singleAuthor = TRUE;
//    } else {
//        singleAuthor = FALSE;
//    }
//
//    hasEditors = FALSE;
//
//    typeData = [ReferencingApp sharedInstance].typeData;
//
//    refDataDictionary  = [[NSMutableDictionary alloc] init];
//
//    for (NSDictionary *dict in typeData)
//    {
//        if ([[dict objectForKey:@"name"] isEqualToString:self.title])
//        {
//            typeDictionary = dict;
//            [refDataDictionary setObject:@"" forKey:[typeDictionary objectForKey:@"fields"]];
//        }
//    }
//
////    if (_currentReference == nil) {
////        authorArray = [[NSMutableArray alloc] initWithObjects:[@{@"Surname":@"",@"Initials":@""} mutableCopy], nil];
////        authorArray = [[NSMutableArray alloc] initWithObjects:[@{@"Firstname":@"",@"Lastname":@""} mutableCopy], nil];
//
////        for (NSDictionary *dict in typeData)
////        {
////            if ([[dict objectForKey:@"name"] isEqualToString:self.title])
////            {
////                typeDictionary = dict;
////                if ([[dict objectForKey:@"name"] isEqualToString:@"Blog"]) {
////                    NSDictionary *ddict = @{@"fields":arr_Fields,@"name":@"Blog"};
////                    typeDictionary = ddict;
////                }
//
////                for (NSString *fields in [typeDictionary objectForKey:@"fields"])
////                {
////                    if ([fields isEqualToString:@"Accessed Date"] || [fields isEqualToString:@"Last Accessed"]) {
////                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
////                        formatter.dateStyle = NSDateFormatterLongStyle;
////                        formatter.timeStyle = NSDateFormatterNoStyle;
////
////                        NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
////                        [refDataDictionary setObject:stringFromDate forKey:fields];
////                    }
////                    else
////                        [refDataDictionary setObject:@"" forKey:fields];
////                }
////            }
////        }
//
////        if ([[typeDictionary allKeys] containsObject:@"hasOnline"]) {
////
////            CGFloat headerHeight;
////            if ([self.title isEqualToString:@"Newspaper"] || [self.title isEqualToString:@"Dissertation"])
////                headerHeight = 40;
////            else
////                headerHeight = 80;
////
////            headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.tableView.frame.size.width,headerHeight)];
////            headerView.clipsToBounds = TRUE;
////            [headerView setBackgroundColor:[UIColor whiteColor]];
////
////            UISegmentedControl *segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Print",@"Online"]];
////            segmentControl.selectedSegmentIndex = 0;
////            [segmentControl addTarget:self action:@selector(indexChanged:) forControlEvents:UIControlEventValueChanged];
////            [segmentControl setFrame:CGRectMake(15, 5, headerView.frame.size.width - 30, 29)];
////            [headerView addSubview:segmentControl];
////
////            NSArray *secondItems;
////            if ([self.title isEqualToString:@"Book"])
////                secondItems = @[@"Author(s)",@"Editor(s)",@"No Author"];
////            else
////                secondItems = @[@"Author(s)",@"Editor(s)"];
////
////            UISegmentedControl *secondSegmentControl = [[UISegmentedControl alloc] initWithItems:secondItems];
////            secondSegmentControl.selectedSegmentIndex = 0;
////            [secondSegmentControl addTarget:self action:@selector(secondIndexChanged:) forControlEvents:UIControlEventValueChanged];
////            [secondSegmentControl setFrame:CGRectMake(15, 40, headerView.frame.size.width - 30, 29)];
////            [headerView addSubview:secondSegmentControl];
////
////            self.tableView.tableHeaderView = headerView;
////        }
////    }
////    else
////    {
////        NSError *error;
////        NSMutableDictionary *refData = [NSJSONSerialization JSONObjectWithData:[_currentReference.data dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
////        NSLog(@"%@",refData);
////
////        NSMutableArray *authorArrayData  = [[refData objectForKey:@"Author Double Field"] mutableCopy];
////
////        authorArray = [[NSMutableArray alloc] init];
////
////        for (NSDictionary *dict in authorArrayData)
////        {
////            [authorArray addObject:[[NSMutableDictionary alloc] initWithDictionary:dict]];
////        }
////
////        for (NSDictionary *dict in typeData)
////        {
////            if ([[dict objectForKey:@"name"] isEqualToString:_currentReference.referenceType])
////            {
////                typeDictionary = dict;
////
////                for (NSString *fields in [dict objectForKey:@"fields"])
////                    [refDataDictionary setObject:[refData objectForKey:fields] forKey:fields];
////            }
////        }
////
////    }
//}
//
//-(void)func_Done_PickerView {
//    [self.txt_ChooseYourCitationStyle resignFirstResponder];
//    self.txt_ChooseYourCitationStyle.text = @"";
//}
//
//-(void)indexChanged:(UISegmentedControl *)sender
//{
//    switch (sender.selectedSegmentIndex)
//    {
//        case 0:
//            self.title = [self.title stringByReplacingOccurrencesOfString:@" Online" withString:@""];
//            self.title = [self.title stringByReplacingOccurrencesOfString:@" No Author" withString:@""];
//
//            CGFloat headerHeight;
//            if ([self.title isEqualToString:@"Newspaper"] || [self.title isEqualToString:@"Dissertation"])
//                headerHeight = 40;
//            else
//                headerHeight = 80;
//
//            [headerView  setFrame:CGRectMake(0,0,self.tableView.frame.size.width,headerHeight)];
//            self.tableView.tableHeaderView = headerView;
//
//            refDataDictionary  = [[NSMutableDictionary alloc] init];
//            for (NSDictionary *dict in typeData)
//            {
//                if ([[dict objectForKey:@"name"] isEqualToString:self.title])
//                {
//                    typeDictionary = dict;
//
//
//                    for (NSString *fields in [dict objectForKey:@"fields"])
//                    {
//                        if ([fields isEqualToString:@"Accessed Date"] || [fields isEqualToString:@"Last Accessed"])
//                        {
//                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                            formatter.dateStyle = NSDateFormatterLongStyle;
//                            formatter.timeStyle = NSDateFormatterNoStyle;
//
//                            NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
//                            [refDataDictionary setObject:stringFromDate forKey:fields];
//                        }
//                        else
//                            [refDataDictionary setObject:@"" forKey:fields];
//
//                    }
//                }
//            }
//
//            [self.tableView reloadData];
//
//
//            break;
//        case 1:
//            self.title = [self.title stringByReplacingOccurrencesOfString:@" No Author" withString:@""];
//            self.title = [self.title stringByAppendingString:@" Online"];
//            [headerView  setFrame:CGRectMake(0,0,self.tableView.frame.size.width,40)];
//            self.tableView.tableHeaderView = headerView;
//
//
//            refDataDictionary  = [[NSMutableDictionary alloc] init];
//            for (NSDictionary *dict in typeData)
//            {
//                if ([[dict objectForKey:@"name"] isEqualToString:self.title])
//                {
//                    typeDictionary = dict;
//
//
//                    for (NSString *fields in [dict objectForKey:@"fields"])
//                    {
//                        if ([fields isEqualToString:@"Accessed Date"] || [fields isEqualToString:@"Last Accessed"])
//                        {
//                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                            formatter.dateStyle = NSDateFormatterLongStyle;
//                            formatter.timeStyle = NSDateFormatterNoStyle;
//
//
//                            NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
//                            [refDataDictionary setObject:stringFromDate forKey:fields];
//                        }
//                        else
//                            [refDataDictionary setObject:@"" forKey:fields];
//
//                    }
//                }
//            }
//
//            [self.tableView reloadData];
//
//            break;
//        default:
//            break;
//    }
//}
//
//-(void)secondIndexChanged:(UISegmentedControl *)sender
//{
//    if ([self.title rangeOfString:@"Book"].location == NSNotFound)
//    {
//        switch (sender.selectedSegmentIndex)
//        {
//            case 0:
//                hasEditors = FALSE;
//                break;
//            case 1:
//                hasEditors = TRUE;
//                break;
//            default:
//                break;
//        }
//    }
//    else
//    {
//        switch (sender.selectedSegmentIndex)
//        {
//            case 0:
//                self.title = [self.title stringByReplacingOccurrencesOfString:@" No Author" withString:@""];
//                hasEditors = FALSE;
//
//                refDataDictionary  = [[NSMutableDictionary alloc] init];
//                for (NSDictionary *dict in typeData)
//                {
//                    if ([[dict objectForKey:@"name"] isEqualToString:self.title])
//                    {
//                        typeDictionary = dict;
//
//
//                        for (NSString *fields in [dict objectForKey:@"fields"])
//                        {
//                            if ([fields isEqualToString:@"Accessed Date"] || [fields isEqualToString:@"Last Accessed"])
//                            {
//                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                                formatter.dateStyle = NSDateFormatterLongStyle;
//                                formatter.timeStyle = NSDateFormatterNoStyle;
//
//
//                                NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
//                                [refDataDictionary setObject:stringFromDate forKey:fields];
//                            }
//                            else
//                                [refDataDictionary setObject:@"" forKey:fields];
//
//                        }
//                    }
//                }
//
//                [self.tableView reloadData];
//                break;
//            case 1:
//                self.title = [self.title stringByReplacingOccurrencesOfString:@" No Author" withString:@""];
//                hasEditors = TRUE;
//
//                refDataDictionary  = [[NSMutableDictionary alloc] init];
//                for (NSDictionary *dict in typeData)
//                {
//                    if ([[dict objectForKey:@"name"] isEqualToString:self.title])
//                    {
//                        typeDictionary = dict;
//
//
//                        for (NSString *fields in [dict objectForKey:@"fields"])
//                        {
//                            if ([fields isEqualToString:@"Accessed Date"] || [fields isEqualToString:@"Last Accessed"])
//                            {
//                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                                formatter.dateStyle = NSDateFormatterLongStyle;
//                                formatter.timeStyle = NSDateFormatterNoStyle;
//
//
//                                NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
//                                [refDataDictionary setObject:stringFromDate forKey:fields];
//                            }
//                            else
//                                [refDataDictionary setObject:@"" forKey:fields];
//
//                        }
//                    }
//                }
//
//                [self.tableView reloadData];
//                break;
//            case 2:
//                self.title = [self.title stringByAppendingString:@" No Author"];
//
//                refDataDictionary  = [[NSMutableDictionary alloc] init];
//                for (NSDictionary *dict in typeData)
//                {
//                    if ([[dict objectForKey:@"name"] isEqualToString:self.title])
//                    {
//                        typeDictionary = dict;
//
//
//                        for (NSString *fields in [dict objectForKey:@"fields"])
//                        {
//                            if ([fields isEqualToString:@"Accessed Date"] || [fields isEqualToString:@"Last Accessed"])
//                            {
//                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                                formatter.dateStyle = NSDateFormatterLongStyle;
//                                formatter.timeStyle = NSDateFormatterNoStyle;
//
//
//                                NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
//                                [refDataDictionary setObject:stringFromDate forKey:fields];
//                            }
//                            else
//                                [refDataDictionary setObject:@"" forKey:fields];
//
//                        }
//                    }
//                }
//
//                [self.tableView reloadData];
//
//                break;
//            default:
//                break;
//        }
//    }
//}
//
//
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//
//
//-(void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//
//    [[IndexViewController sharedIndexVC] hideTopMenu];
//    self.navigationController.navigationBar.barTintColor = [self.view.tintColor colorWithAlphaComponent:0.7];
//    self.navigationController.navigationBar.backgroundColor = [self.view.tintColor colorWithAlphaComponent:0.5];
//
////    [self.tabBarController.tabBar setHidden:YES];
//}
//
//#pragma mark - Table view data source
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    if ([[[typeDictionary objectForKey:@"fields"] objectAtIndex:0] isEqualToString:@"Author Double Field"])  {
//        return 2;
//    }
//
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    // Return the number of rows in the section.
//    if ([[[typeDictionary objectForKey:@"fields"] objectAtIndex:0] isEqualToString:@"Author Double Field"]) {
//        if (section == 0)
//            return  [authorArray count];
//        return [[typeDictionary objectForKey:@"fields"] count] - 1;
//    }
//
//    if([[typeDictionary objectForKey:@"name"] isEqualToString:@"Website"]) {//additional textfield for Website type
//        return [[typeDictionary objectForKey:@"fields"] count] + 1;
//    }
//
//
//    return [[typeDictionary objectForKey:@"fields"] count];
//}
//
//
//
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 50;
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ([[[typeDictionary objectForKey:@"fields"] objectAtIndex:0] isEqualToString:@"Author Double Field"]) {
//        if (indexPath.section == 0) {
//            DoubleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"doubleCell" forIndexPath:indexPath];
//            cell.delegate = self;
////            cell.firstTextField.placeholder = [NSString stringWithFormat:@"Firstname"];
////            cell.firstTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Firstname"];
////            cell.secondTextField.placeholder = [NSString stringWithFormat:@"Lastname"];
////            cell.secondTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Lastname"];
//
//            cell.firstTextField.placeholder = [NSString stringWithFormat:@"Surname"];
//            cell.firstTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Surname"];
//            cell.secondTextField.placeholder = [NSString stringWithFormat:@"Initial(s)"];
//            cell.secondTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Initials"];
//
//            if (singleAuthor == TRUE)
//                [cell.addRemoveButton setHidden:TRUE];
//
//            [cell.addRemoveButton setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
//            [cell.addRemoveButton setFrame:CGRectMake(283, 14, 22, 22)];
//
//            if (indexPath.row != 0) {
//                cell.firstTextField.placeholder = [NSString stringWithFormat:@"Surname %i",indexPath.row + 1];
//                cell.firstTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Surname"];
//                cell.secondTextField.placeholder = [NSString stringWithFormat:@"Initial(s) %i",indexPath.row + 1];
//                cell.secondTextField.text = [[authorArray objectAtIndex:indexPath.row] objectForKey:@"Initial(s)"];
//
//                [cell.addRemoveButton setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
//                [cell.addRemoveButton setFrame:CGRectMake(283, 14, 22, 22)];
//            }
//            return cell;
//        } else {
//            FullWidthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fullWidthCell" forIndexPath:indexPath];
////            cell.delegate = self;
//            cell.textField.placeholder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row + 1]];
//
//            cell.textField.keyboardType = UIKeyboardTypeDefault;
//
//            if ([cell.textField.placeholder isEqualToString:@"Year"])
//                cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//            if ([cell.textField.placeholder isEqualToString:@"URL"] || [cell.textField.placeholder isEqualToString:@"Image URL"] || [cell.textField.placeholder isEqualToString:@"Video URL"] || [cell.textField.placeholder isEqualToString:@"Website"])
//                cell.textField.keyboardType = UIKeyboardTypeURL;
//
//            NSString *str_Fields = [NSString stringWithFormat:@"%@",[refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row + 1]]];
//            NSLog(@"%@",str_Fields);
//
//            if ([str_Fields isEqual:@"(null)"]) {
//                cell.textField.text = @"";
//            } else {
//                cell.textField.text = str_Fields;
//            }
//
//            cell.textField.tag = indexPath.row + 1;
//            cell.textField.delegate = self;
//
////            [cell.textField removeTarget:self action:@selector(func_TextField:) forControlEvents:UIControlEventEditingDidBegin];
////            [cell.textField addTarget:self action:@selector(func_TextField:) forControlEvents:UIControlEventEditingDidBegin];
//
//            return cell;
//        }
//    } else {
//        NSString *typeName = [typeDictionary objectForKey:@"name"];
//        if([typeName isEqualToString:@"Website"]) {
//            if(indexPath.row == 0) {
//                SearchBarTableViewCell *result = [tableView dequeueReusableCellWithIdentifier:@"SearchBarTableViewCell" forIndexPath:indexPath];
//                result.delegate = self;
//
//                NSString *strVal = [refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row]];
//                if(strVal == nil) {
//                    result.searchField.text = @"";
//                } else {
//                    result.searchField.text = strVal;
//                }
//
//                //result.searchField.text = @"https://www.macworld.com/article/3137575/macs/the-new-macbook-pro-isnt-just-a-laptop-its-a-strategy-shift.html";
//                //result.searchField.text = @"https://www.macrumors.com/roundup/ipad/";
//
//                return result;
//            } else {
//                FullWidthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fullWidthCell" forIndexPath:indexPath];
//                cell.delegate = self;
//                cell.textField.placeholder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row-1]];
//
//                cell.textField.keyboardType = UIKeyboardTypeDefault;
//
//                if ([cell.textField.placeholder isEqualToString:@"Year"])
//                    cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//                if ([cell.textField.placeholder isEqualToString:@"URL"] || [cell.textField.placeholder isEqualToString:@"Image URL"] || [cell.textField.placeholder isEqualToString:@"Video URL"] || [cell.textField.placeholder isEqualToString:@"Website"])
//                    cell.textField.keyboardType = UIKeyboardTypeURL;
//
//                cell.textField.text = [NSString stringWithFormat:@"%@",[refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row-1]]];
//                return cell;
//            }
//        } else {
//            FullWidthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fullWidthCell" forIndexPath:indexPath];
//            cell.delegate = self;
//            cell.textField.placeholder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row]];
//
//            cell.textField.keyboardType = UIKeyboardTypeDefault;
//
//            if ([cell.textField.placeholder isEqualToString:@"Year"])
//                cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//            if ([cell.textField.placeholder isEqualToString:@"URL"] || [cell.textField.placeholder isEqualToString:@"Image URL"] || [cell.textField.placeholder isEqualToString:@"Video URL"] || [cell.textField.placeholder isEqualToString:@"Website"])
//                cell.textField.keyboardType = UIKeyboardTypeURL;
//
//            cell.textField.text = [NSString stringWithFormat:@"%@",[refDataDictionary objectForKey:[[typeDictionary objectForKey:@"fields"] objectAtIndex:indexPath.row]]];
//
//            return cell;
//        }
//    }
//}
//
//-(void)addButtonPressedAtCell:(DoubleTableViewCell *)cell
//{
//    [self.view endEditing:TRUE];
//
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
//
//    if (indexPath.row == 0) {
//        [authorArray addObject:[@{@"Surname":@"",@"Initials":@""} mutableCopy]];
//        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[authorArray count] - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
//    } else {
//        [authorArray removeObjectAtIndex:indexPath.row];
//        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationTop];
//        [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:0.3f];
//    }
//
//}
//
//
//-(void)projectSelectorDidSelectProject:(Project *)project {
//    _currentProject = project;
//    [self saveButtonPressed:nil];
//}
//
//
//
//- (IBAction)btn_ChooseYourCitationStyle:(id)sender {
//    UIPickerView *objPickerView = [UIPickerView new];
//
//    objPickerView.delegate = self;
//    objPickerView.dataSource = self;
//
//    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
//    [toolBar setTintColor:[UIColor grayColor]];
//    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btn_done_ToolBar)];
//    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
//
//    [self.txt_ChooseYourCitationStyle setInputAccessoryView:toolBar];
//}
//
//-(void)picker_Recieve :(NSNotification *) notification {
//    _txt_ChooseYourCitationStyle.text = [NSString stringWithFormat:@"%@", notification.object];
//}
//
//-(void) func_save_API {
////        NSArray *arr_AutherField = [refDataDictionary objectForKey:@"Author Double Field"];
////        NSString *str_firstName = [[arr_AutherField objectAtIndex:0] objectForKey:@"Firstname"];
////        NSString *str_lastname = [[arr_AutherField objectAtIndex:0] objectForKey:@"Lastname"];
////
////        NSString *str_BLOG_TITLE = [refDataDictionary objectForKey:@"BLOG TITLE"];
////        NSString *str_DATE_ACCESSED_VIEWED = [refDataDictionary objectForKey:@"DATE ACCESSED/VIEWED"];
////        NSString *str_DATE_PUBLISHED = [refDataDictionary objectForKey:@"DATE PUBLISHED"];
////        NSString *str_TITLE_OF_POST = [refDataDictionary objectForKey:@"TITLE OF POST"];
////        NSString *str_URL = [refDataDictionary objectForKey:@"URL"];
//
//        NSArray *arr_AutherField = [refDataDictionary objectForKey:@"Author Double Field"];
//        NSString *str_firstName = [[arr_AutherField objectAtIndex:0] objectForKey:@"Initials"];
//        NSString *str_lastname = [[arr_AutherField objectAtIndex:0] objectForKey:@"Surname"];
//
//        NSString *str_BLOG_TITLE = [refDataDictionary objectForKey:@"Blog Title"];
//        NSString *str_DATE_ACCESSED_VIEWED = [refDataDictionary objectForKey:@"Accessed Date"];
//        NSString *str_DATE_PUBLISHED = [refDataDictionary objectForKey:@"Date of Post"];
//        NSString *str_TITLE_OF_POST = [refDataDictionary objectForKey:@"Title of blog entry"];
//        NSString *str_URL = [refDataDictionary objectForKey:@"URL"];
//
//        NSMutableDictionary *dic_JSON = [[NSMutableDictionary alloc]init];
//
//        [dic_JSON setObject:@"ITEM-1" forKey:@"id"];
//        [dic_JSON setObject:@"post-weblog" forKey:@"type"];
//
//        NSDictionary *dict_Author = @{@"family":str_firstName,@"given":str_lastname};
//        NSArray *arr_Author = @[dict_Author];
//        [dic_JSON setObject:arr_Author forKey:@"author"];
//
//        NSArray *arr_DateAccessed = [str_DATE_ACCESSED_VIEWED componentsSeparatedByString:@","];
//        NSArray *arr_DateParts = @[arr_DateAccessed];
//        NSDictionary *dict_Dateparts = @{@"date-parts":arr_DateParts};
//        [dic_JSON setObject:dict_Dateparts forKey:@"issued"];
//        [dic_JSON setObject:str_TITLE_OF_POST forKey:@"title"];
//        [dic_JSON setObject:str_BLOG_TITLE forKey:@"container-title"];
//        [dic_JSON setObject:str_URL forKey:@"URL"];
//
//        NSArray *arr_DatePublished = [str_DATE_PUBLISHED componentsSeparatedByString:@","];
//
//        NSArray *arr_DateParts_Accessed = @[arr_DatePublished];
//        NSDictionary *dict_Dateparts_Accessed = @{@"date-parts":arr_DateParts_Accessed};
//
//        [dic_JSON setObject:dict_Dateparts_Accessed forKey:@"accessed"];
//
//        NSArray *arr_JSON = [NSArray arrayWithObject:dic_JSON];
//        NSLog(@"%@",arr_JSON);
//
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr_JSON options:NSJSONWritingPrettyPrinted error:nil];
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        [self func_POST_API:jsonString];
//}
//
//- (IBAction)saveButtonPressed:(id)sender {
//    [self.view endEditing:TRUE];
//
////    if (_currentProject == nil) {
////        ProjectSelectorViewController *selector = [self.storyboard instantiateViewControllerWithIdentifier:@"projectSelectorVC"];
////        selector.delegate = self;
////        [self presentViewController:selector animated:true completion:nil];
////        return;
////    }
////
////    if (_currentReference != nil)
////    {
////        if ([self hasEmptyCells])
////            return;
////
////        if ([_currentProject.onlineProject boolValue] == TRUE)
////        {
////            loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
////            [self.view.window addSubview:loadingView];
////            [loadingView fadeIn];
////
////            NSError *error;
////
////            if ([refDataDictionary objectForKey:@"Author Double Field"] != nil)
////                [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
////            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
////            {
////                NSMutableDictionary *refData = [NSJSONSerialization JSONObjectWithData:[_currentReference.data dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
////                [refDataDictionary setObject:[refData objectForKey:@"hasEditors"]  forKey:@"hasEditors"];
////
////            }
////
////
////            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
////
////            [[ReferencingApp sharedInstance] editReferenceWithData:dataString referenceType:_currentReference.referenceType projectID:_currentReference.projectID andReferenceID:_currentReference.referenceID];
////
////
////        }
////        else
////        {
////            NSError *error;
////            if ([refDataDictionary objectForKey:@"Author Double Field"] != nil)
////                [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
////            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
////            {
////                NSMutableDictionary *refData = [NSJSONSerialization JSONObjectWithData:[_currentReference.data dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
////                [refDataDictionary setObject:[refData objectForKey:@"hasEditors"]  forKey:@"hasEditors"];
////            }
////
////            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
////            _currentReference.data = dataString;
////
////            [[ReferencingApp sharedInstance] saveDatabase];
////
//////            [[IndexViewController sharedIndexVC] saveSucessfull];
////
////            /*********
////             *********
////             *********
////             *********
////             *********
////             *********/
//////            [self.navigationController popViewControllerAnimated:true];
////
////        }
////    }
////    else
////    {
////        if ([self hasEmptyCells])
////            return;
////
////        if ([_currentProject.onlineProject boolValue] == TRUE)
////        {
////            NSError *error;
////            [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
////            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
////                [refDataDictionary setObject:[NSNumber numberWithBool:hasEditors] forKey:@"hasEditors"];
////
////            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
////
////            loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
////            [self.view.window addSubview:loadingView];
////            [loadingView fadeIn];
////
////            [[ReferencingApp sharedInstance] createReferenceWithData:dataString referenceType:[typeDictionary objectForKey:@"name"] andProjectID:_currentProject.projectID];
////
////
////        } else {
////            AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
////            NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
////            Reference *newRef = [NSEntityDescription insertNewObjectForEntityForName:@"Reference" inManagedObjectContext:managedObjectContext];
////
////            NSError *error;
////            [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
////            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
////                [refDataDictionary setObject:[NSNumber numberWithBool:hasEditors] forKey:@"hasEditors"];
////
////            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
////
////            newRef.dateCreated = [NSDate date];
////            newRef.data = dataString;
////            newRef.referenceType = [typeDictionary objectForKey:@"name"];
////
////            _currentReference = newRef;
////
////            [_currentProject addReferencesObject:newRef];
////
////            [[ReferencingApp sharedInstance] saveDatabase];
////
////            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
////            for (UIViewController *aViewController in allViewControllers) {
////                if ([aViewController isKindOfClass:[ProjectReferencesTableViewController class]]) {
//////                    [[IndexViewController sharedIndexVC] saveSucessfull];
//////                    [self.navigationController popToViewController:aViewController animated:TRUE];
//////                    return;
////                    break;
////                }
////            }
////
////
//////            [[IndexViewController sharedIndexVC] saveSucessfull];
////            /*********
////             *********
////             *********
////             *********
////             *********
////             *********/
//////            [self.navigationController popViewControllerAnimated:true];
////        }
////
////    }
//    [self func_save_API];
//}
//
//-(void)func_Back {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[IndexViewController sharedIndexVC] saveSucessfull];
//        [self.navigationController popViewControllerAnimated:YES];
//    });
//}
//
//-(void) func_Save_Local_DB:(NSString*)result_API {
//    if (_currentProject == nil) {
//        ProjectSelectorViewController *selector = [self.storyboard instantiateViewControllerWithIdentifier:@"projectSelectorVC"];
//        selector.delegate = self;
//        [self presentViewController:selector animated:true completion:nil];
//        return;
//    }
//
//    if (_currentReference != nil) {
//        if ([self hasEmptyCells])
//            return;
//
//        if ([_currentProject.onlineProject boolValue] == TRUE)
//        {
//            loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
//            [self.view.window addSubview:loadingView];
//            [loadingView fadeIn];
//
//            NSError *error;
//
//            if ([refDataDictionary objectForKey:@"Author Double Field"] != nil)
//                [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"]) {
//                NSMutableDictionary *refData = [NSJSONSerialization JSONObjectWithData:[_currentReference.data dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
//                [refDataDictionary setObject:[refData objectForKey:@"hasEditors"]  forKey:@"hasEditors"];
//            }
//            [refDataDictionary setObject:result_API  forKey:@"result_API"];
//
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//            dateFormatter.dateFormat = @"yyyyMMDDhhmmss";
//            _currentReference.projectID = [dateFormatter stringFromDate:[NSDate date]];
//
//            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
//
//            [[ReferencingApp sharedInstance] editReferenceWithData:dataString referenceType:_currentReference.referenceType projectID:_currentReference.projectID andReferenceID:_currentReference.referenceID];
//            [self func_Back];
//        } else {
//            NSError *error;
//            if ([refDataDictionary objectForKey:@"Author Double Field"] != nil)
//                [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
//            {
//                NSMutableDictionary *refData = [NSJSONSerialization JSONObjectWithData:[_currentReference.data dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
//                [refDataDictionary setObject:[refData objectForKey:@"hasEditors"]  forKey:@"hasEditors"];
//            }
//            [refDataDictionary setObject:result_API  forKey:@"result_API"];
//
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//            dateFormatter.dateFormat = @"yyyyMMDDhhmmss";
//            _currentReference.projectID = [dateFormatter stringFromDate:[NSDate date]];
//
//            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
//
//            _currentReference.data = dataString;
//
//            [[ReferencingApp sharedInstance] saveDatabase];
//
//            [self func_Back];
//            //            [[IndexViewController sharedIndexVC] saveSucessfull];
//
//            /*********
//             *********
//             *********
//             *********
//             *********
//             *********/
//            //            [self.navigationController popViewControllerAnimated:true];
//        }
//    }
//    else
//    {
//        if ([self hasEmptyCells])
//            return;
//
//        if ([_currentProject.onlineProject boolValue] == TRUE)
//        {
//            NSError *error;
//            [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
//                [refDataDictionary setObject:[NSNumber numberWithBool:hasEditors] forKey:@"hasEditors"];
//            [refDataDictionary setObject:result_API  forKey:@"result_API"];
//            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
//
//            loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
//            [self.view.window addSubview:loadingView];
//            [loadingView fadeIn];
//
//            [[ReferencingApp sharedInstance] createReferenceWithData:dataString referenceType:[typeDictionary objectForKey:@"name"] andProjectID:_currentProject.projectID];
//            [self func_Back];
//        } else {
//            AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//            NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
//            Reference *newRef = [NSEntityDescription insertNewObjectForEntityForName:@"Reference" inManagedObjectContext:managedObjectContext];
//
//            NSError *error;
//            [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//            if ([self.title isEqualToString:@"Journal"] || [self.title isEqualToString:@"Book"]|| [self.title isEqualToString:@"Encyclopedia"])
//                [refDataDictionary setObject:[NSNumber numberWithBool:hasEditors] forKey:@"hasEditors"];
//            [refDataDictionary setObject:result_API  forKey:@"result_API"];
//            NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
//
//            newRef.dateCreated = [NSDate date];
//            newRef.data = dataString;
//            newRef.referenceType = [typeDictionary objectForKey:@"name"];
//
//            _currentReference = newRef;
//
//            [_currentProject addReferencesObject:newRef];
//
//            [[ReferencingApp sharedInstance] saveDatabase];
//
//            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//            for (UIViewController *aViewController in allViewControllers) {
//                if ([aViewController isKindOfClass:[ProjectReferencesTableViewController class]]) {
//                    //                    [[IndexViewController sharedIndexVC] saveSucessfull];
//                    //                    [self.navigationController popToViewController:aViewController animated:TRUE];
//                    //                    return;
//                    break;
//                }
//            }
//
//            [self func_Back];
//            //            [[IndexViewController sharedIndexVC] saveSucessfull];
//            /*********
//             *********
//             *********
//             *********
//             *********
//             *********/
//            //            [self.navigationController popViewControllerAnimated:true];
//        }
//
//    }
//}
//
//-(BOOL)hasEmptyCells
//{
//    for (NSString *key in refDataDictionary )
//    {
//        NSString *checkForEmptyString = [refDataDictionary objectForKey:key];
//
//        if ([key isEqualToString:@"Author Double Field"])
//        {
//            for (NSMutableDictionary *authorDict in authorArray)
//            {
//                for (NSString *authorString in [authorDict allValues])
//                {
//                    if ([authorString isEqualToString:@""])
//                    {
//                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                                       message: @"Please fill the fields."
//                                                                      delegate: self
//                                                             cancelButtonTitle:@"OK"
//                                                             otherButtonTitles:nil];
//                        [alert show];
//                        return TRUE;
//                    }
//                }
//            }
//        }
//        else if ([checkForEmptyString isEqualToString:@""] &&  ![key isEqualToString:@"Edition"]  && ![key isEqualToString:@"Issue"] && ![key isEqualToString:@"Web URL"])
//        {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                           message: @"Please fill the fields."
//                                                          delegate: self
//                                                 cancelButtonTitle:@"OK"
//                                                 otherButtonTitles:nil];
//            [alert show];
//            return TRUE;
//        }
//    }
//
//    return FALSE;
//}
//
//-(void)editingEndedAtCell:(FullWidthTableViewCell *)cell {
//    [refDataDictionary setObject:cell.textField.text forKey:cell.textField.placeholder];
//}
//
//-(void)surnameEndEditingAtCell:(DoubleTableViewCell *)cell {
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
////    [[authorArray objectAtIndex:indexPath.row] setObject:cell.firstTextField.text forKey:@"Firstname"];
//    [[authorArray objectAtIndex:indexPath.row] setObject:cell.firstTextField.text forKey:@"Surname"];
//
//    NSLog(@"%@",authorArray);
//    [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//}
//
//-(void)initialsEndEditingAtCell:(DoubleTableViewCell *)cell {
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
////    [[authorArray objectAtIndex:indexPath.row] setObject:cell.secondTextField.text forKey:@"Lastname"];
//    [[authorArray objectAtIndex:indexPath.row] setObject:cell.secondTextField.text forKey:@"Initials"];
//
//    NSLog(@"%@",authorArray);
//    [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
//}
//
//-(void)unloadLoadingView
//{
//    [UIView animateWithDuration:0.5f animations:^{
//        loadingView.alpha = 0.0f;
//    } completion:^(BOOL finished) {
//        [loadingView removeFromSuperview];
//        loadingView = nil;
//
//    }];
//}
//
//#pragma mark REFERECING APP DELEGATE
//
//-(void)referenceEditingFailedWithErrror:(NSString *)errorString
//{
//    [self.tableView reloadData];
//
//    [self unloadLoadingView];
//
//
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                   message: errorString
//                                                  delegate: self
//                                         cancelButtonTitle:@"OK"
//                                         otherButtonTitles:nil];
//    [alert show];
//}
//
//-(void)referenceCreationFailedWithEror:(NSString *)errorString
//{
//    [self unloadLoadingView];
//
//
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                   message: errorString
//                                                  delegate: self
//                                         cancelButtonTitle:@"OK"
//                                         otherButtonTitles:nil];
//    [alert show];
//}
//
//
//-(void)referenceSucessfullyEdited:(NSDictionary *)dictionary
//{
//
//    _currentReference.data = [dictionary objectForKey:@"data"];
//
//    [self unloadLoadingView];
//
//    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//    for (UIViewController *aViewController in allViewControllers) {
//        if ([aViewController isKindOfClass:[ProjectReferencesTableViewController class]]) {
//            [[IndexViewController sharedIndexVC] saveSucessfull];
//
//            [self.navigationController popToViewController:aViewController animated:TRUE];
//            return;
//
//        }
//    }
//
//
//
//    [[IndexViewController sharedIndexVC] saveSucessfull];
//
//    [self.navigationController popViewControllerAnimated:true];
//
//
//
//}
//
//-(void)referenceSucessfullyCreated:(NSDictionary *)dictionary
//{
//    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//    NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
//
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Reference" inManagedObjectContext:managedObjectContext];
//    Reference *newRef = [[Reference alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
//
//    newRef.projectID = [[dictionary objectForKey:@"project_id"] stringValue];
//    newRef.referenceType = [dictionary objectForKey:@"reference_type"];
//    newRef.data = [dictionary objectForKey:@"data"];
//    newRef.referenceID = [[dictionary objectForKey:@"id"] stringValue];
//
//    NSString *dateString = [dictionary objectForKey:@"created_at"];
//    dateString = [dateString substringToIndex:[dateString length]-5];
//
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-M-d'T'H:m:s"];
//
//    newRef.dateCreated = [dateFormat dateFromString:dateString];
//
//    _currentReference = newRef;
//
//    [_currentProject addReferencesObject:newRef];
//
//    [self unloadLoadingView];
//
//    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//    for (UIViewController *aViewController in allViewControllers) {
//        if ([aViewController isKindOfClass:[ProjectReferencesTableViewController class]]) {
//            [[IndexViewController sharedIndexVC] saveSucessfull];
//
//            [self.navigationController popToViewController:aViewController animated:TRUE];
//            return;
//        }
//    }
//    [[IndexViewController sharedIndexVC] saveSucessfull];
//    [self.navigationController popViewControllerAnimated:true];
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    NSString *str_PlaceHolder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:textField.tag]];
//    NSLog(@"%@",str_PlaceHolder);
//
////    if ([str_PlaceHolder isEqualToString:@"DATE PUBLISHED"]) {
//    if ([str_PlaceHolder isEqualToString:@"Date of Post"]) {
//
////    } else if ([str_PlaceHolder isEqualToString:@"DATE ACCESSED/VIEWED"]) {
//        } else if ([str_PlaceHolder isEqualToString:@"Accessed Date"]) {
//
//    } else {
//        [refDataDictionary setObject:textField.text forKey:textField.placeholder];
//    }
//
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    return YES;
//}
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//        NSString *str_PlaceHolder = [NSString stringWithFormat:@"%@",[[typeDictionary objectForKey:@"fields"] objectAtIndex:textField.tag]];
//        NSLog(@"%@",str_PlaceHolder);
//
////        if ([str_PlaceHolder isEqualToString:@"DATE PUBLISHED"]) {
//        if ([str_PlaceHolder isEqualToString:@"Date of Post"]) {
//            UIDatePicker *date_picker ;
//            date_picker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
//            [date_picker setDatePickerMode:UIDatePickerModeDate];
//            [date_picker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
//            date_picker.tag = 0;
//            textField.inputView = date_picker;
//
//            UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
//            [toolBar setTintColor:[UIColor grayColor]];
//            UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btn_done_ToolBar)];
//            UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//            [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
//
//            [textField setInputAccessoryView:toolBar];
////        } else if ([str_PlaceHolder isEqualToString:@"DATE ACCESSED/VIEWED"]) {
////        } else if ([str_PlaceHolder isEqualToString:@"Accessed Date"]) {
//        } else if ([str_PlaceHolder isEqualToString:@"Accessed Date"]) {
//            UIDatePicker *date_picker;
//            date_picker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
//            [date_picker setDatePickerMode:UIDatePickerModeDate];
//            [date_picker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
//            date_picker.tag = 1;
//            textField.inputView = date_picker;
//
//            UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
//            [toolBar setTintColor:[UIColor grayColor]];
//
//            UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btn_done_ToolBar)];
//            UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//            [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
//
//            [textField setInputAccessoryView:toolBar];
//        }
//    }
//
//-(void)btn_done_ToolBar {
//    [_tableView reloadData];
//}
//
//- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker {
//    NSDateFormatter * formatter =  [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy,MM,dd"];
//    NSString *dateString = [formatter stringFromDate:datePicker.date];
//
//    NSLog(@"date selected %@",dateString);
//    if (datePicker.tag == 0) {
////        [refDataDictionary setObject:dateString forKey:@"DATE PUBLISHED"];
//        [refDataDictionary setObject:dateString forKey:@"Date of Post"];
//    } else if (datePicker.tag == 1) {
////        [refDataDictionary setObject:dateString forKey:@"DATE ACCESSED/VIEWED"];
//        [refDataDictionary setObject:dateString forKey:@"Accessed Date"];
//    }
//
//    NSLog(@"%@",refDataDictionary);
//}
//
//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    textField.text = [textField.text capitalizedString];
//    return true;
//}
//
//#pragma - SearchBarTableViewCellDelegate
//-(void)didEndEditingSearchBarCell:(SearchBarTableViewCell *)cell strValue:(NSString *) strValue {
//    if(strValue != nil && strValue.length > 0) {
//        NSString *tempStrURL = [strValue lowercaseString];
//
//        if(![tempStrURL hasPrefix:@"http://"]){
//            if(![tempStrURL hasPrefix:@"https://"]){
//                tempStrURL = [NSString stringWithFormat:@"http://%@", tempStrURL];
//            }
//        }
//
//        if([Utilities validateUrl:tempStrURL]) {
//            loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
//            [self.view.window addSubview:loadingView];
//            [loadingView fadeIn];
//
//            WebDataManager *wdm = [[WebDataManager alloc] init];
//            wdm.delegate = self;
//            [wdm extractDataFromURL:tempStrURL];
//        } else {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                           message: @"Please enter valid web url."
//                                                          delegate: self
//                                                 cancelButtonTitle:@"OK"
//                                                 otherButtonTitles:nil];
//            [alert show];
//        }
//    } else {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
//                                                       message: @"Please enter web url."
//                                                      delegate: self
//                                             cancelButtonTitle:@"OK"
//                                             otherButtonTitles:nil];
//        [alert show];
//    }
//
//}
//
//- (NSString *) removeToken:(NSString *) strToken from:(NSString *) strString
//{
//    strString = [strString substringFromIndex:strToken.length];
//
//    if([strString containsString:@"."]){
//        NSRange range = [strString rangeOfString:@"."];
//        if(range.location != NSNotFound){
//            strString = [strString substringToIndex:range.location];
//        }
//    }
//
//    return strString;
//}
//
//- (NSString *) validateAuthor:(NSString *) strAuthor strURL:(NSString *) strURL
//{
//    if(strAuthor == nil || strAuthor.length == 0){
//        NSURL *url = [NSURL URLWithString:strURL];
//        strAuthor = [url host];
//
//        if([strAuthor hasPrefix:@"www."]){
//            strAuthor = [self removeToken:@"www." from:strAuthor];
//        }
//        else if([strAuthor hasPrefix:@"http://www."]){
//            strAuthor = [self removeToken:@"http://www." from:strAuthor];
//        }
//        else if([strAuthor hasPrefix:@"https://www."]){
//            strAuthor = [self removeToken:@"https://www." from:strAuthor];
//        }
//        else if([strAuthor hasPrefix:@"http://"]){
//            strAuthor = [self removeToken:@"http://" from:strAuthor];
//        }
//        else if([strAuthor hasPrefix:@"https://"]){
//            strAuthor = [self removeToken:@"https://" from:strAuthor];
//        }
//
//
//        strAuthor = [NSString stringWithFormat:@"%@%@",[[strAuthor substringToIndex:1] uppercaseString],[strAuthor substringFromIndex:1] ];
//    }
//
//    return strAuthor;
//}
//
//#pragma mark - WebDataManagerDelegate
//
//-(void)didExtractedDataFromURL:(WebDataManager *)dataManager dictionary:(NSDictionary *) responseDictionary
//{
//
//    if(responseDictionary != nil) {
//
//        NSDictionary *webData = responseDictionary;
//        NSString *strTitle = [webData objectForKey:@"title"];
//        NSString *strURL = [webData objectForKey:@"url"];
//        NSString *strAuthor = [webData objectForKey:@"author"];
//        NSString *strDate = [webData objectForKey:@"date"];
//
//        strAuthor = [self validateAuthor:strAuthor strURL:strURL];
//
//        NSString *lastAccessed = [Utilities dateToString:[NSDate date] format:@"dd MMMM YYYY"];
//        NSArray* fields = [typeDictionary objectForKey:@"fields"];
//        for(int i = 0; i < fields.count; ++i){
//            NSString *strKey = [fields objectAtIndex:i];
//            if([strKey isEqualToString:@"Title"]) {
//                [refDataDictionary setObject:strTitle forKey:strKey];
//            }
//            else if([strKey isEqualToString:@"URL"]){
//                [refDataDictionary setObject:strURL forKey:strKey];
//            }
//            else if([strKey isEqualToString:@"Last Accessed"]){
//                [refDataDictionary setObject:lastAccessed forKey:strKey];
//            }
//            else if([strKey isEqualToString:@"Year"]){
//                if(strDate != nil) {
//                    [refDataDictionary setObject:strDate forKey:strKey];
//                }
//            }
//            else if([strKey isEqualToString:@"Name of Website/Author"]){
//                if(strAuthor != nil) {
//                    [refDataDictionary setObject:strAuthor forKey:strKey];
//                }
//            }
//
//        }
//
//        [refDataDictionary setObject:strURL forKey:@"Web URL"];
//
//        [self.tableView reloadData];
//        [self unloadLoadingView];
//    }
//    else{
//        [self unloadLoadingView];
//    }
//}
//
//
//
//- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
//    return 1;
//}
//
//- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
//    return _arr_Style_Picker.count;
//}
//
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    return [self.arr_Style_Picker[row] uppercaseString];
//}
//
//- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
//    NSLog(@"%@",self.arr_Style_Picker[row]);
//    self.txt_ChooseYourCitationStyle.text = self.arr_Style_Picker[row];
//}
//
//
//
//-(void) func_get_styles {
//    [MBProgressHUD showHUDAddedTo:self.view animated:true];
//    NSString *full_URL = [NSString stringWithFormat:@"http://3.19.62.211:5000/get_styles"];
//    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:full_URL]];
//
//    //    NSString *userUpdate =[NSString stringWithFormat:@"user=%@&password=%@&api_id=%@&to=%@&text=%@",_username,_password,_apiID,[_numbers componentsJoinedByString:@","],_txtMsg.text, nil];
//
//    //create the Method "GET" or "POST"
//    [urlRequest setHTTPMethod:@"POST"];
//
//    //Convert the String to Data
//    //    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
//
//    //Apply the data to the body
//    //    [urlRequest setHTTPBody:data1];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
////        NSLog(@"httpResponse is:- %ld",(long)httpResponse.statusCode);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
//
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//        if(httpResponse.statusCode == 200) {
//            NSError *parseError = nil;
//            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//            NSInteger success = [[responseDictionary objectForKey:@"status"] integerValue];
//            if(success == 1) {
//                NSArray *arrStyles = [responseDictionary objectForKey:@"styles"];
//                for (NSString *strStyle in arrStyles) {
//                    NSMutableString *strStyle_Filter = [[NSMutableString alloc]init];
//                    strStyle_Filter = [[strStyle stringByReplacingOccurrencesOfString:@"/home/ubuntu/.local/lib/python2.7/site-packages/citeproc/data/styles/dependent/" withString:@""] mutableCopy];
//                    strStyle_Filter = [[strStyle_Filter stringByReplacingOccurrencesOfString:@".csl" withString:@""] mutableCopy];
//                    [self.arr_Style_Picker addObject:strStyle_Filter];
//                }
//            }
//        }
//    }];
//    [dataTask resume];
//}
//
//
//
//-(void)func_get: (NSString*)json {
//    //    NSString *str_Param = [NSString stringWithFormat:@"json:%@&style_name:%@",json,self.txt_ChooseYourCitationStyle.text];
//    //    NSString *param = [NSString stringWithFormat:@"json=%@&style_name=%@",json,self.txt_ChooseYourCitationStyle.text];
//    //    NSLog(@"str_Param \n%@",param);
//    //    NSData *postData = [param dataUsingEncoding:NSUTF8StringEncoding];
//
//    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://3.19.62.211:5000/get"]];
//
//    NSString *userUpdate =[NSString stringWithFormat:@"json=json&style_name=future-virology"];
//
//    //create the Method "GET" or "POST"
//    [urlRequest setHTTPMethod:@"POST"];
//
//    //Convert the String to Data
//    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
//
//    //Apply the data to the body
//    [urlRequest setHTTPBody:data1];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//        if(httpResponse.statusCode == 200)
//        {
//            NSError *parseError = nil;
//            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//            NSLog(@"The response is - %@",responseDictionary);
//            NSInteger success = [[responseDictionary objectForKey:@"success"] integerValue];
//            if(success == 1)
//            {
//                NSLog(@"Login SUCCESS");
//            }
//            else
//            {
//                NSLog(@"Login FAILURE");
//            }
//        }
//        else
//        {
//            NSLog(@"Error");
//        }
//    }];
//
//    return;
//
//
////
////
////    NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
////                               @"User-Agent": @"PostmanRuntime/7.17.1",
////                               @"Accept": @"*/*",
////                               @"Cache-Control": @"no-cache",
//////                               @"Postman-Token": @"bf5d93a5-0f94-4703-8fd1-dc78e54af53e,a155e00c-df58-4e5d-9af9-2832ca75a698",
////                               @"Host": @"3.19.62.211:5000",
////                               @"Content-Type": @"multipart/form-data; boundary=--------------------------053104914594977153577004",
////                               @"Accept-Encoding": @"gzip, deflate",
//////                               @"Content-Length": @"666",
////                               @"Connection": @"keep-alive",
////                               @"cache-control": @"no-cache" };
////
////    NSDictionary *dict_Param = [[NSDictionary alloc] initWithObjectsAndKeys:@"json", @"json",
////                                self.txt_ChooseYourCitationStyle.text, @"style_name",
////                                nil];
////    NSLog(@"dict_Param is- %@",dict_Param);
////    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict_Param options:0 error:nil];
////
////    NSString *full_URL = [NSString stringWithFormat:@"http://3.19.62.211:5000/get"];
//////    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:full_URL]];
////        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:full_URL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:600.0];
////
////    [urlRequest setHTTPBody:postData];
////    [urlRequest setHTTPMethod:@"POST"];
//////    [urlRequest setAllHTTPHeaderFields:headers];
//////    [urlRequest addValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
//////    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
////    [urlRequest addValue:@"text/html; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
////    [urlRequest addValue:@"multipart/form-data; boundary=--------------------------053104914594977153577004" forHTTPHeaderField:@"Content-Type"];
////    [urlRequest addValue:@"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" forHTTPHeaderField:@"Content-Type"];
////
////
////
////    NSURLSession *session = [NSURLSession sharedSession];
////    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
////        if (error == nil) {
////            NSLog(@"error is:- %@",error);
////        }
////
////        //        NSLog(@"response is:- %@",response);
////        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
////        NSLog(@"httpResponse is:- %@",httpResponse);
////
////        if(httpResponse.statusCode == 200) {
////            NSError *parseError = nil;
////            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
////            NSLog(@"response is:- %@",responseDictionary);
////            NSInteger success = [[responseDictionary objectForKey:@"status"] integerValue];
////
////            if(success == 1) {
////                //                NSArray *arrStyles = [responseDictionary objectForKey:@"styles"];
////
////                //                for (NSString *strStyle in arrStyles) {
////                //                    NSMutableString *strStyle_Filter = [[NSMutableString alloc]init];
////                //                    strStyle_Filter = [[strStyle stringByReplacingOccurrencesOfString:@"/home/ubuntu/.local/lib/python2.7/site-packages/citeproc/data/styles/" withString:@""] mutableCopy];
////                //                    strStyle_Filter = [[strStyle_Filter stringByReplacingOccurrencesOfString:@".csl" withString:@""] mutableCopy];
////                //                    [self.arr_Style_Picker addObject:strStyle_Filter];
////                //                }
////            }
////        }
////    }];
////    [dataTask resume];
//}
//
//
//
////- (void) func_get:(NSString*)json {
//////    NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
//////                               @"User-Agent": @"PostmanRuntime/7.17.1",
//////                               @"Accept": @"*/*",
//////                               @"Cache-Control": @"no-cache",
//////                               @"Postman-Token": @"bf5d93a5-0f94-4703-8fd1-dc78e54af53e,a155e00c-df58-4e5d-9af9-2832ca75a698",
//////                               @"Host": @"3.19.62.211:5000",
//////                               @"Content-Type": @"multipart/form-data; boundary=--------------------------053104914594977153577004",
//////                               @"Accept-Encoding": @"gzip, deflate",
//////                               @"Content-Length": @"666",
//////                               @"Connection": @"keep-alive",
//////                               @"cache-control": @"no-cache" };
////
////    NSDictionary *dict_Param = [[NSDictionary alloc] initWithObjectsAndKeys: json, @"json",
////                                self.txt_ChooseYourCitationStyle.text, @"style_name",
////                                nil];
////    NSLog(@"dict_Param is- %@",dict_Param);
////
//////    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict_Param options:0 error:nil];
//////    NSString *full_URL = [NSString stringWithFormat:@"http://3.19.62.211:5000/get"];
////
////    NSString * strServiceURL = [NSString stringWithFormat:@"http://3.19.62.211:5000/get"];
////
////    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
////    manager.requestSerializer = [AFJSONRequestSerializer serializer];
////    manager.responseSerializer = [AFJSONResponseSerializer serializer];
////    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
////
////    [manager POST:strServiceURL parameters:dict_Param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//////        if (fileData!=nil){
//////            [formData appendPartWithFileData:fileData name:@"Key name on which you will upload file" fileName:@"Image name" mimeType:@"image/jpeg"];
//////        }
////    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
////        NSLog(@"Success: %@", responseObject);
//////        block(responseObject,nil);
////    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
////        NSLog(@"Error: %@", error);
//////        block (nil , error);
////    }];
////}
//
//
//
////- (void) func_get:(NSString*)json {
////    NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
////                               @"User-Agent": @"PostmanRuntime/7.17.1",
////                               @"Accept": @"*/*",
////                               @"Cache-Control": @"no-cache",
////                               @"Postman-Token": @"bf5d93a5-0f94-4703-8fd1-dc78e54af53e,a155e00c-df58-4e5d-9af9-2832ca75a698",
////                               @"Host": @"3.19.62.211:5000",
////                               @"Content-Type": @"multipart/form-data; boundary=--------------------------053104914594977153577004",
////                               @"Accept-Encoding": @"gzip, deflate",
////                               @"Content-Length": @"666",
////                               @"Connection": @"keep-alive",
////                               @"cache-control": @"no-cache" };
////
////
////
//////    NSArray *parameters = @[ @{ @"name": @"json", @"value": @"[
//////                                {
//////                                    \"author\" : [
//////                                    {
//////                                        \"given\" : \"iOS\",
//////                                        \"family\" : \"Raja\"
//////                                    }
//////                                    ],
//////                                    \"issued\" : {
//////                                    \"date-parts\" : [
//////                                    \"2018 10 1\"
//////                                    ]
//////                                },
//////                                \"id\" : \"ITEM-1\",
//////                                \"title\" : \"Post\",
//////                                \"container-title\" : \"Blog\",
//////                                \"type\" : \"post-weblog\",
//////                                \"accessed\" : {
//////                                \"date-parts\" : [
//////                                \"2018 10 1\"
//////                                ]
//////                                },
//////                             \"URL\" : \"Www.App.Com\"
//////                             }
//////                             ]" },
//////    @{ @"name": @"style_name", @"value": @"future-virology" }];
////
////
////
////    NSString *boundary = @"----WebKitFormBoundary7MA4YWxkTrZu0gW";
////
////    NSDictionary *dict_Param = [[NSDictionary alloc] initWithObjectsAndKeys: json, @"json",
////                                self.txt_ChooseYourCitationStyle.text, @"style_name",
////                                nil];
////    NSLog(@"dict_Param is- %@",dict_Param);
////
////    NSError *error;
////    NSMutableString *body = [NSMutableString new];
////    for (NSDictionary *param in dict_Param) {
////        [body appendFormat:@"--%@\r\n", boundary];
////        if (param[@"fileName"]) {
////            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"; filename=\"%@\"\r\n", param[@"name"], param[@"fileName"]];
////            [body appendFormat:@"Content-Type: %@\r\n\r\n", param[@"contentType"]];
////            [body appendFormat:@"%@", [NSString stringWithContentsOfFile:param[@"fileName"] encoding:NSUTF8StringEncoding error:&error]];
////            if (error) {
////                NSLog(@"%@", error);
////            }
////        } else {
////            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"\r\n\r\n", param[@"name"]];
////            [body appendFormat:@"%@", param[@"value"]];
////        }
////    }
////    [body appendFormat:@"\r\n--%@--\r\n", boundary];
////
////
////    NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding];
////
////    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://3.19.62.211:5000/get"]
////                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
////                                                       timeoutInterval:10.0];
////    [request setHTTPMethod:@"POST"];
////    [request setAllHTTPHeaderFields:headers];
////    [request setHTTPBody:postData];
////
////    NSURLSession *session = [NSURLSession sharedSession];
////    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
////                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
////                                                    if (error) {
////                                                        NSLog(@"%@", error);
////                                                    } else {
////                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
////                                                        NSLog(@"%@", httpResponse);
////                                                    }
////                                                }];
////    [dataTask resume];
////}
//
//-(void) func_POST_API: (NSString*)json {
//    [MBProgressHUD showHUDAddedTo:self.view animated:true];
//    NSDictionary *dict_Param = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                json, @"json",
//                                self.str_Selected_Style, @"style",
//                                nil];
//    NSLog(@"dict_Param is- %@",dict_Param);
//    NSString *boundary = [self generateBoundaryString];
//
//    // configure the request
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://ondemandcab.com/appentus_food/api/csl_v1/get"]];
//    [request setHTTPMethod:@"POST"];
//    // set content type
//
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//    // create body
//    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:dict_Param paths:nil fieldName:nil];
//    NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
//    NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
////        NSLog(@"response is:- %@",response);
////        if (error) {
////            NSLog(@"error = %@", error);
////            return;
////        }
//
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//        if(httpResponse.statusCode == 200) {
//            NSError *parseError = nil;
//            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//            NSInteger success = [[responseDictionary objectForKey:@"status"] integerValue];
//            if(success == 1) {
//                NSString *str_result = [responseDictionary objectForKey:@"result"];
//                NSLog(@"result is:- %@",str_result);
//
//                NSString *str_MSG = [NSString stringWithFormat:@"%@",str_result];
//                NSLog(@"%@",str_MSG);
//
////                NSLog(@"%@",[self convertHTML:str_MSG]);
//
//                [self func_Save_Local_DB:str_MSG];
//
////                dispatch_async(dispatch_get_main_queue(), ^{
////                    UILabel *lbl_HTML = [[UILabel alloc]initWithFrame:CGRectMake(0, 100,300, 100)];
//
////                    lbl_HTML.numberOfLines = 50;
////                    lbl_HTML.backgroundColor = [UIColor redColor];
////                    lbl_HTML.attributedText = [self convertHTML:str_MSG];
////
////                    [self.view addSubview:lbl_HTML];
////                    [self.view bringSubviewToFront:lbl_HTML];
//
////                    [[IndexViewController sharedIndexVC] saveSucessfull];
////                    [self.navigationController popViewControllerAnimated:YES];
////                });
//
////                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:str_MSG preferredStyle:UIAlertControllerStyleAlert];
////                UIAlertAction *ok_Action = [UIAlertAction actionWithTitle:@"0K" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
////                    [self.navigationController popViewControllerAnimated:YES];
////                }];
////                [alert addAction:ok_Action];
////                [self presentViewController:alert animated:YES completion:nil];
//            } else {
//                NSString *str_error = [responseDictionary objectForKey:@"error"];
//                NSLog(@"str_error is:- %@",str_error);
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[IndexViewController sharedIndexVC] error_POPUP:str_error];
//                });
//            }
//        }
////        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
////        NSLog(@"result = %@", result);
//    }];
//    [task resume];
//}
//
//- (NSData *)createBodyWithBoundary:(NSString *)boundary
//                        parameters:(NSDictionary *)parameters
//                             paths:(NSArray *)paths
//                         fieldName:(NSString *)fieldName {
//    NSMutableData *httpBody = [NSMutableData data];
//
//    // add params (all params are strings)
//
//    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
//        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
//        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
//    }];
//
//    // add image data
//
//    for (NSString *path in paths) {
//        NSString *filename  = [path lastPathComponent];
//        NSData   *data      = [NSData dataWithContentsOfFile:path];
//        NSString *mimetype  = [self mimeTypeForPath:path];
//
//        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
//        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
//        [httpBody appendData:data];
//        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    }
//
//    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    return httpBody;
//}
//
//- (NSString *)mimeTypeForPath:(NSString *)path {
//    // get a mime type for an extension using MobileCoreServices.framework
//
//    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
//    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
//    assert(UTI != NULL);
//
//    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
//    assert(mimetype != NULL);
//
//    CFRelease(UTI);
//
//    return mimetype;
//}
//
//- (NSString *)generateBoundaryString {
//    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
//}
//
//
//-(NSAttributedString *)convertHTML:(NSString *)html {
//   return [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]
//                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
//                                               NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
//                          documentAttributes:nil error:nil];
//
//
////    NSScanner *myScanner;
////    NSString *text = nil;
////    myScanner = [NSScanner scannerWithString:html];
////
////    while ([myScanner isAtEnd] == NO) {
////
////        [myScanner scanUpToString:@"<" intoString:NULL] ;
////
////        [myScanner scanUpToString:@">" intoString:&text] ;
////
////        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
////    }
////    //
////    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
////
////    return html;
//}
//
//@end
