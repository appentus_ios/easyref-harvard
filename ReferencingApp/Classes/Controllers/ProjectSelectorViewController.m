//
//  ProjectSelectorViewController.m
//  EasyRef
//
//  Created by Radu Mihaiu on 18/01/2016.
//  Copyright © 2016 SquaredDimesions. All rights reserved.
//

#import "ProjectSelectorViewController.h"
#import "ProjectTableViewCell.h"
#import "ReferencingApp.h"


@interface ProjectSelectorViewController () {
  NSArray *projectsArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ProjectSelectorViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    
  _tableView.delegate = self;
  _tableView.dataSource = self;
  projectsArray = [[ReferencingApp sharedInstance]  getDataBaseWithName:@"Project"];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 55.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
  if (section == 0) {
    return @"Local";
  }
  return [ReferencingApp sharedInstance].user.name;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  if ([ReferencingApp sharedInstance].isOffline == true) {
    return 1;
  }
  return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if (section == 0) {
    return  [projectsArray count];
  }
  return [[ReferencingApp sharedInstance].onlineProjectsArray count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  ProjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"projectCell" forIndexPath:indexPath];
  cell.coverView.userInteractionEnabled = false;
  
  if (indexPath.section == 0) {
    Project *project = [projectsArray objectAtIndex:indexPath.row];
    cell.projectTextField.text = project.name;
//    cell.numberLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[project.references count]];
      cell.numberLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self func_get_reference:project.name]];
  } else {
    Project *project = [[ReferencingApp sharedInstance].onlineProjectsArray objectAtIndex:indexPath.row];
    cell.projectTextField.text = project.name;
//    cell.numberLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[project.references count]];
      cell.numberLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[self func_get_reference:project.name]];
  }
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@",[self current_ref]);
    NSLog(@"%@",[self.current_ref referenceID]);
    
  if ([_delegate respondsToSelector:@selector(projectSelectorDidSelectProject:)]) {
      Project *project = [projectsArray objectAtIndex:indexPath.row];
      [self func_update_In_Local_DB:project.name];
      
      if (indexPath.section == 0) {
          
      }
//      [_delegate projectSelectorDidSelectProject:[projectsArray objectAtIndex:indexPath.row]];
    else
      [_delegate projectSelectorDidSelectProject:[[ReferencingApp sharedInstance].onlineProjectsArray objectAtIndex:indexPath.row]];
  }
  
  [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
      [self dismissViewControllerAnimated:true completion:nil];
}

-(NSUInteger) func_get_reference :(NSString *)str_projectname {
    NSMutableArray *arr_AllProjects = [[NSMutableArray alloc]init];
    NSArray *arr_fetch = [[ReferencingApp sharedInstance] getDataBaseWithName:@"Reference"];
    NSLog(@"arr_fetch is:- %@",arr_fetch);
    
    for (Reference *ref in arr_fetch) {
        NSLog(@"ref %@",ref);
        NSLog(@"%@",ref.data);
        
        if ([[str_projectname lowercaseString] isEqualToString:[ref.projectID lowercaseString]]) {
            [arr_AllProjects addObject:ref];
        }
    }
    return [arr_AllProjects count];
}



-(void) func_update_In_Local_DB:(NSString *)str_projectID {
    NSMutableDictionary *dict_save_local_DB = [[NSMutableDictionary alloc]init];
    
    [dict_save_local_DB setValue:str_projectID forKey:@"projectID"];
    [dict_save_local_DB setValue:[self.current_ref data] forKey:@"data"];
    [dict_save_local_DB setValue:[self.current_ref referenceType] forKey:@"referenceType"];
    [dict_save_local_DB setValue:[self.current_ref referenceID] forKey:@"referenceID"];
    
    [[ReferencingApp sharedInstance] update_Datebase:dict_save_local_DB];
}

@end
