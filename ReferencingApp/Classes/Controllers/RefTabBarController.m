//  RefTabBarController.m
//  ReferencingApp
//  Created by Radu Mihaiu on 24/08/2014.
//  Copyright (c) 2014 SquaredDimesions. All rights reserved.



#import "RefTabBarController.h"
#import "JSON_Param.h"

@interface RefTabBarController () <UITabBarControllerDelegate,UITabBarDelegate>

@end

@implementation RefTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[JSON_Param sharedInstance]func_selected_tabbar:@"0"];
    self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    NSString *str_selectedIndex = [NSString stringWithFormat:@"%lu",(unsigned long)tabBarController.selectedIndex];
    [[JSON_Param sharedInstance]func_selected_tabbar:str_selectedIndex];
    
//    NSLog(@"%@",[[JSON_Param sharedInstance] str_selected_tabbar]);
}


@end


