//  ReferencingApp.m
//  ReferencingApp
//  Created by Radu Mihaiu on 14/09/2014.
//  Copyright (c) 2014 SquaredDimesions. All rights reserved.



#import "ReferencingApp.h"
#import "AFNetworking/AFNetworking.h"

#define kAPIBaseURLString @"http://reference-app.herokuapp.com/"
#define kWorldCatAPIKey @"lRrBaDb27qnHagjExrzG9FxHKbqGxyZeUm5YorgTjZbggyKLhDCVnMzEXf1rXfGuf6Z2BLnOUW0rBwtK"

#define k_DATE_PUBLISHED @"Date Published"
#define k_TITLE_OF_REPORT @"Title of report"
#define k_PUBLISHER @"Publisher"
#define k_PAGE_RANGE @"Page Range"
#define k_WEBSITE_NAME @"Website name"
#define k_URL @"URL"
#define k_DATE_ACCESSED_VIEWED @"Date Accessed Viewed"
#define k_ANNOTATION @"Annotation"

#define k_TITLE_OF_ARTWORK @"Title Of Artwork"
#define k_MEDIUM @"Medium"

#define k_TITLE_OF_POST @"Title Of Post"
#define k_BLOG_TITLE @"Blog Title"

#define k_TITLE_OF_BOOK @"Title Of Book"
#define k_EDITION @"Edition"
#define k_PUBLISHER @"Publisher"
#define k_PUBLISHER_PLACE @"Publisher Place"
#define k_PAGE_RANGE @"Page Range"


#define k_TITLE_OF_CHAPTER @"Title Of Chapter"
#define k_BOOK_AUTHOR @"Book Author"
#define k_PAGE_RANGE @"Page Range"

#define k_TITLE_OF_REVIEW @"Title Of Review"
#define k_AUTHOR_OF_ORIGINAL_BOOK @"Author Of Original Book"
#define k_DATE_ORIGINAL_BOOK_PUBLISHED @"Date Original Book Published"
#define k_TITLE_OF_ORIGINAL_BOOK @"Title Of Original Book"

#define k_TITLE_OF_ORIGINAL_BOOK @"Title Of Original Book"
#define k_TITLE_OF_PAPER @"Title Of Paper"
#define k_NAME_OF_JOURNAL_PROCEEDINGS @"Name Of Journal Proceedings"

#define k_PUBLICATION_TITLE @"Publication Title"
#define k_TITLE_OF_ARTICLE @"Title Of Article"
#define k_DATABASE_NAME @"Database Name"

#define k_NAME_OF_DICTIONARY @"Name Of Dictionary"
#define k_TITLE_OF_Entry @"Title Of Entry"

#define k_TITLE_OF_E_BOOK @"Title Of E-Book"
#define k_NAME_OF_ENCYCLOPEDIA @"Name Of Encyclopedia"

#define k_TITLE_OF_FILM_MOVIE @"Title Of Film/Movie"
#define k_DISTRIBUTOR @"Distributor"

#define k_TITLE_OR_DESCRIPTION @"Title Or Description"
#define k_CONTAINER @"Container"

#define k_INTERVIEWER @"Interviewer"
#define k_DATE_OF_INTERVIEW @"Date Of Interview"
#define k_TITLE_OF_INTERVIEW @"Title Of Interview"

#define k_NAME_OF_JOURNAL @"Name Of Journal"
#define k_VOLUME_NUMBER @"Volume Number"
#define k_ISSUE_NUMBER @"Issue Number"
#define k_DOI @"DOI"

#define k_DATE_ISSUED @"Date Issued"
#define k_TITLE_OF_BILL @"Title Of Bill"

#define k_CASE_NAME @"Case Name"
#define k_DATE_OF_DECISION @"Date Of Decision"

#define k_TITLE_OF_LEGISLATION @"Title Of Legislation"

#define k_NAME_OF_MAGAZINE @"Name Of Magazine"

#define k_TITLE_OF_LEGISLATION @"Title Of Legislation"

#define k_TITLE_OF_MAP @"Title Of Map"

#define k_NAME_OF_NEWS_SOURCE @"Name Of News Source"

#define k_TITLE_OF_PATENT @"Title Of Patent"

#define k_DATE_OF_COMMUNICATION @"Date Of Communication"
#define k_TITLE_OF_COMMUNICATION @"Title Of Comminication"

static ReferencingApp *sharedInstance;

@implementation ReferencingApp

+(void)start {
    ReferencingApp *newRefApp = [[ReferencingApp alloc] init];
    newRefApp.isOffline = FALSE;
    [newRefApp initialize];
    sharedInstance = newRefApp;
}

+(ReferencingApp *)sharedInstance {
    return sharedInstance;
}

-(void) initialize {
    self.user = [[User alloc] init];
    _onlineProjectsArray = [[NSMutableArray alloc] init];
    _linkedScanRefArray = [[NSMutableArray alloc] init];
    _projectsLeftToSync = 0;
    
    _typeData = @[
                 @{@"name":@"Artwork",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_ARTWORK,k_MEDIUM,k_URL,k_DATE_ACCESSED_VIEWED,k_ANNOTATION]},
                 @{@"name":@"Blog Post",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_POST,k_BLOG_TITLE,k_URL,k_DATE_ACCESSED_VIEWED,k_ANNOTATION]},
                 @{@"name":@"Book",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_BOOK,k_EDITION,k_PUBLISHER,k_PUBLISHER_PLACE,k_PAGE_RANGE,k_ANNOTATION]},
                 
                 @{@"name":@"Book Chapter",@"fields":@[k_TITLE_OF_CHAPTER,k_BOOK_AUTHOR,k_DATE_PUBLISHED,k_TITLE_OF_BOOK,k_PUBLISHER,k_PUBLISHER_PLACE,k_PAGE_RANGE,k_ANNOTATION]},
                 @{@"name":@"Book Review",@"fields":@[k_TITLE_OF_REVIEW,k_PAGE_RANGE,k_AUTHOR_OF_ORIGINAL_BOOK,k_DATE_ORIGINAL_BOOK_PUBLISHED,k_TITLE_OF_ORIGINAL_BOOK,k_ANNOTATION]},
                 @{@"name":@"Conference Paper",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_PAPER,k_NAME_OF_JOURNAL_PROCEEDINGS,k_PUBLISHER,k_ANNOTATION]},
              
                 @{@"name":@"Database",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_ARTICLE,k_PUBLICATION_TITLE,k_DATABASE_NAME,k_ANNOTATION]},
                 @{@"name":@"Dictionary Entry",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_Entry,k_NAME_OF_DICTIONARY,k_PAGE_RANGE,k_ANNOTATION]},
                 @{@"name":@"E-book",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_E_BOOK,k_PAGE_RANGE,k_URL,k_DATE_ACCESSED_VIEWED,k_ANNOTATION]},
               
                 @{@"name":@"Encyclopedia Entry",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_Entry,k_NAME_OF_ENCYCLOPEDIA,k_PAGE_RANGE,k_ANNOTATION]},
                 @{@"name":@"Film/Movie",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_FILM_MOVIE,k_MEDIUM,k_DISTRIBUTOR,k_ANNOTATION]},
               
                 @{@"name":@"Image",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OR_DESCRIPTION,k_CONTAINER,k_ANNOTATION]},
                 @{@"name":@"Interview",@"fields":@[k_INTERVIEWER,k_DATE_OF_INTERVIEW,k_TITLE_OF_INTERVIEW,k_ANNOTATION]},
                 @{@"name":@"Journal Article",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_ARTICLE,k_NAME_OF_JOURNAL,k_VOLUME_NUMBER,k_ISSUE_NUMBER,k_PAGE_RANGE,k_URL,k_DOI,k_ANNOTATION]},
                
                 @{@"name":@"Legal Bill",@"fields":@[k_DATE_ISSUED,k_TITLE_OF_BILL,k_ANNOTATION]},
                 @{@"name":@"Legal Case",@"fields":@[k_CASE_NAME,k_DATE_OF_DECISION,k_ANNOTATION]},
                 @{@"name":@"Legislation",@"fields":@[k_TITLE_OF_LEGISLATION,k_DATE_PUBLISHED,k_ANNOTATION]},
                
                 @{@"name":@"Magazine Article",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_ARTICLE,k_NAME_OF_MAGAZINE,k_PAGE_RANGE,k_ANNOTATION]},
                 @{@"name":@"Map",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_MAP,k_ANNOTATION]},
                 @{@"name":@"News Article",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_ARTICLE,k_NAME_OF_NEWS_SOURCE,k_PAGE_RANGE,k_URL,k_DATE_ACCESSED_VIEWED,k_ANNOTATION]},
                 
                 @{@"name":@"Patent",@"fields":@[k_DATE_ISSUED,k_TITLE_OF_PATENT,k_ANNOTATION]},
                 @{@"name":@"Personal Communication",@"fields":@[k_DATE_OF_COMMUNICATION,k_TITLE_OF_COMMUNICATION,k_ANNOTATION]},
                 @{@"name":@"Regulation",@"fields":@[k_DATE_PUBLISHED,@"Title Of Regulation",k_ANNOTATION]},
                
                 @{@"name":@"Report",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_REPORT,k_PUBLISHER,k_PAGE_RANGE,@"Website Name",k_URL,k_DATE_ACCESSED_VIEWED,k_ANNOTATION]},
                 @{@"name":@"Review",@"fields":@[k_DATE_PUBLISHED,k_TITLE_OF_REVIEW,k_ANNOTATION]},
                 @{@"name":@"Song",@"fields":@[@"Date Produced",@"Title Of Song",k_ANNOTATION]},
                 
                 @{@"name":@"Speech",@"fields":@[k_DATE_PUBLISHED,@"Title Of Speech",k_ANNOTATION]},
                 @{@"name":@"Standard",@"fields":@[k_DATE_PUBLISHED,@"Title Of Standard",k_ANNOTATION]},
                 @{@"name":@"Thesis (or Dissertation)",@"fields":@[k_DATE_PUBLISHED,@"Title Of Thesis/Dissertation",k_PAGE_RANGE,k_ANNOTATION]},
                
                 @{@"name":@"TV/Radio Broadcast",@"fields":@[@"Date Of BROADCAST",@"Title Of BROADCAST/Programme/Network",k_ANNOTATION]},
                 @{@"name":@"Video",@"fields":@[@"Title Of Video",k_DATE_PUBLISHED,k_PUBLISHER,@"Format",k_URL,k_DATE_ACCESSED_VIEWED,k_ANNOTATION]},
                 @{@"name":@"Website",@"fields":@[k_DATE_PUBLISHED,@"Title Of Article/Page",@"Website Name",k_PUBLISHER,k_URL,k_DATE_ACCESSED_VIEWED,k_ANNOTATION]},
                 ];
    
    
    
    _referenceTypeArray = @[
                          @{ @"colorValue":@0x609eab , @"imgValue":[UIImage imageNamed:@"annual-report"], @"name":@"Artwork"},
                          @{ @"colorValue":@0x7ab7c5 , @"imgValue":[UIImage imageNamed:@"audio-cd"], @"name":@"Blog Post"},
                          @{ @"colorValue":@0x86cbda , @"imgValue":[UIImage imageNamed:@"blog"], @"name":@"Book"},
                          @{ @"colorValue":@0x377c86 , @"imgValue":[UIImage imageNamed:@"book"], @"name":@"Book Chapter"},
                          @{ @"colorValue":@0x377c86 , @"imgValue":[UIImage imageNamed:@"book"], @"name":@"Book Review"},
                          @{ @"colorValue":@0x377c86 , @"imgValue":[UIImage imageNamed:@"book"], @"name":@"Conference Paper"},
                          @{ @"colorValue":@0x5196a0 , @"imgValue":[UIImage imageNamed:@"dissertation"], @"name":@"Database"},
                          @{ @"colorValue":@0x5196a0 , @"imgValue":[UIImage imageNamed:@"dissertation"], @"name":@"Dictionary Entry"},
                          @{ @"colorValue":@0x58a5b1 , @"imgValue":[UIImage imageNamed:@"email"], @"name":@"E-book"},
                          @{ @"colorValue":@0x619b52 , @"imgValue":[UIImage imageNamed:@"encyclopedia"], @"name":@"Encyclopedia Entry"},
                          @{ @"colorValue":@0x7ab56c , @"imgValue":[UIImage imageNamed:@"film"], @"name":@"Film/Movie"},
                          @{ @"colorValue":@0x87c876 , @"imgValue":[UIImage imageNamed:@"gov-document"], @"name":@"Image"},
                          @{ @"colorValue":@0x9eab39 , @"imgValue":[UIImage imageNamed:@"image"], @"name":@"Interview"},
                          @{ @"colorValue":@0xb7c552 , @"imgValue":[UIImage imageNamed:@"interview"], @"name":@"Journal Article"},
                          @{ @"colorValue":@0xcbda5a , @"imgValue":[UIImage imageNamed:@"journal"], @"name":@"Legal Bill"},
                          @{ @"colorValue":@0xcbda5a , @"imgValue":[UIImage imageNamed:@"journal"], @"name":@"Legal Case"},
                          @{ @"colorValue":@0xc4ab31 , @"imgValue":[UIImage imageNamed:@"kindle"], @"name":@"Legislation"},
                          @{ @"colorValue":@0xdec54a , @"imgValue":[UIImage imageNamed:@"map"], @"name":@"Magazine Article"},
                          @{ @"colorValue":@0xf6da51 , @"imgValue":[UIImage imageNamed:@"mobile-app"], @"name":@"Map"},
                          @{ @"colorValue":@0xc27029 , @"imgValue":[UIImage imageNamed:@"newspaper"], @"name":@"News Article"},
                          @{ @"colorValue":@0xc27029 , @"imgValue":[UIImage imageNamed:@"newspaper"], @"name":@"Patent"},
                          
                          @{ @"colorValue":@0xc27029 , @"imgValue":[UIImage imageNamed:@"newspaper"], @"name":@"Personal Communication"},
                          @{ @"colorValue":@0xdb8a42 , @"imgValue":[UIImage imageNamed:@"pdf"], @"name":@"Regulation"},
                          @{ @"colorValue":@0xf39848 , @"imgValue":[UIImage imageNamed:@"podcast"], @"name":@"Report"},
                          @{ @"colorValue":@0x994e85 , @"imgValue":[UIImage imageNamed:@"powerpoint"], @"name":@"Review"},
                          @{ @"colorValue":@0xb2689e , @"imgValue":[UIImage imageNamed:@"video"], @"name":@"Song"},
                          @{ @"colorValue":@0xc672af , @"imgValue":[UIImage imageNamed:@"website"], @"name":@"Speech"},
                          @{ @"colorValue":@0x994e85 , @"imgValue":[UIImage imageNamed:@"powerpoint"], @"name":@"Standard"},
                          @{ @"colorValue":@0xb2689e , @"imgValue":[UIImage imageNamed:@"video"], @"name":@"Thesis"},
                          @{ @"colorValue":@0xc672af , @"imgValue":[UIImage imageNamed:@"website"], @"name":@"TV/Radio"},
                          @{ @"colorValue":@0x994e85 , @"imgValue":[UIImage imageNamed:@"powerpoint"], @"name":@"Video"},
                          @{ @"colorValue":@0xb2689e , @"imgValue":[UIImage imageNamed:@"video"], @"name":@"Website"},
                          ];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstRun"]) {
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"isFirstRun"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
        NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
        
        Project *newProject = [NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:managedObjectContext];
        newProject.onlineProject = [NSNumber numberWithBool:FALSE];
        newProject.name = @"Uncategorized";
        newProject.projectID = @"1";
        
        [self saveDatabase];
    }
}



-(void)saveDatabase
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate saveContext];
}

-(void)update_Datebase:(NSDictionary*)dict {
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Reference" inManagedObjectContext:context];
//    Project *newProject = [NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:context];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"referenceID = %@",[dict objectForKey:@"referenceID"]];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectID = 201910290015803"];
    
    fetchRequest.entity = entity;
    
    [fetchRequest setPredicate:predicate];
//    [fetchRequest setFetchLimit:1];
//    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *arrResult = [context executeFetchRequest:fetchRequest error:&error];
    if (arrResult.count == 0) {
        return;
    }
    NSManagedObject *objectManaged = [arrResult objectAtIndex:0];
    [objectManaged setValue:[dict objectForKey:@"data"] forKey:@"data"];
    [objectManaged setValue:[dict objectForKey:@"projectID"] forKey:@"projectID"];
    
    [appDelegate saveContext];
}

-(void)delete_From_Datebase:(NSString*)referenceID {
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Reference" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"referenceID = %@",referenceID];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
    }
}

-(void)save_Local_Database:(NSDictionary*)dict {
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *transaction = [NSEntityDescription insertNewObjectForEntityForName:@"Reference" inManagedObjectContext:context];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"yyyyMMDDhhmmss";
        NSString *str_Date = [dateFormatter stringFromDate:[NSDate date]];
        
        [transaction setValue:[dict objectForKey:@"projectID"] forKey:@"projectID"];
        [transaction setValue:[dict objectForKey:@"data"] forKey:@"data"];
        //    [transaction setValue:[dict objectForKey:@""] forKey:@"dateCreated"];
        [transaction setValue:str_Date forKey:@"referenceID"];
        [transaction setValue:[dict objectForKey:@"referenceType"] forKey:@"referenceType"];
        // Save the context
        
        NSError *error = nil;
        [context save:&error];
        
//        if (![context save:&error]) {
//            NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
//        } else {
//            NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
//        }
    });
}



#pragma mark DATABASE METHODS
-(NSArray *)getDataBaseWithName:(NSString *)name {
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    //2
    NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateCreated" ascending:NO];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    fetchRequest.sortDescriptors = @[sort];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:name
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error;
    
    NSArray *fetchedRecords = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSLog(@"%@",fetchedRecords);
    
    return fetchedRecords;
}

#pragma  mark NETWORK METHODS

-(void)refreshUserAfterDelay {
  int64_t delayInSeconds = 1.0;
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    [[ReferencingApp sharedInstance] getUserData];
  });
}


-(void)getUserForID:(int)userID
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%i",kAPIBaseURLString,@"users/show/",userID]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
   
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];

             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             
             if ([[parsedObject objectForKey:@"errorCode"] intValue] == 0)
             {
                 
             }
             
             
             NSLog(@"%@",parsedObject);
             
             
         }
     }];
}


-(void)loginUserWithEmail:(NSString *)email andPassword:(NSString *)password
{
    NSString *parameterString = [NSString stringWithFormat:@"users/login/?email=%@&password=%@",email,password];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];

    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];

             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(accountSucessfullyCreatedWithData:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         
                         //Your code goes in here
                         [_delegate loginSucessfullWithData:[parsedObject objectForKey:@"data"]];
                         
                     }];
                     
                     
                 }
                 
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(accountCreationFailedWithError:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate loginFailedWithError:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }

             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(loginFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate loginFailedWithError:@"Connection error"];
                 }];
             }

         }
         
     }];
    

}



-(void)createUserWithName:(NSString *)name Password:(NSString *)password Email:(NSString *)email
{
    
    name = [name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    NSString *parameterString = [NSString stringWithFormat:@"users/new/?name=%@&password=%@&email=%@",name,password,email];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                if( [_delegate respondsToSelector:@selector(accountSucessfullyCreatedWithData:)])
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                        [_delegate accountSucessfullyCreatedWithData:[parsedObject objectForKey:@"data"]];
                    }];

                  
                }
                 
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(accountCreationFailedWithError:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate accountCreationFailedWithError:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(accountCreationFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate accountCreationFailedWithError:@"Connection error"];
                 }];
             }
             
         }

     }];

}


-(void)editProjectWithID:(NSString *)projectID andNewName:(NSString *)projectName
{
    projectName = [projectName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    NSString *parameterString = [NSString stringWithFormat:@"projects/edit/%@?name=%@",projectID,projectName];
    
    NSLog(@"%@",parameterString);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(projectScuessfullyEdited)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate projectScuessfullyEdited];
                     }];
                 }
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(projectEditingFailedWithError:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate projectEditingFailedWithError:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(projectEditingFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate projectEditingFailedWithError:@"Connection error"];
                 }];
             }
             
         }

         
         
     }];

}

-(void)deleteProjectWithID:(NSString *)projectID {
    NSString *parameterString = [NSString stringWithFormat:@"projects/delete/%@",projectID];
    
    NSLog(@"%@",parameterString);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(projectSucessfullyDeleted)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate projectSucessfullyDeleted];
                     }];
                 }
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(projectDeletionFailedWithError:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate projectDeletionFailedWithError:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(projectDeletionFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate projectDeletionFailedWithError:@"Connection error"];
                 }];
             }
             
         }

         
     }];
    
}

-(void)createProjectWithName:(NSString *)projectName
{
    
    projectName = [projectName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    NSString *parameterString = [NSString stringWithFormat:@"projects/new/?name=%@&user_id=%@",projectName,_user.userID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(projectSucessfullyCreatedWithData:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate projectSucessfullyCreatedWithData:[parsedObject objectForKey:@"data"]];
                     }];
                 }
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(projectCreationFailedWithError:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate projectCreationFailedWithError:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(projectCreationFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate projectCreationFailedWithError:@"Connection error"];
                 }];
             }
             
         }
         
     }];
}

-(void)deleteReferenceWithID:(NSString *)referenceID {
    NSString *parameterString = [NSString stringWithFormat:@"references/delete/%@",referenceID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];

             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(referenceSucessfullyDeleted)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate referenceSucessfullyDeleted];
                     }];
                     
                     
                 }
                 
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(referenceDeletionFailedWithError:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate referenceDeletionFailedWithError:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(referenceDeletionFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate referenceDeletionFailedWithError:@"Connection error"];
                 }];
             }
             
         }

     }];

    

}



-(void)switchReferenceWithID:(NSString *)referenceID andType:(NSString *)referenceType toProject:(NSString *)newProjectID
{
    NSError * error = nil;
    NSString *parameterString = [NSString stringWithFormat:@"references/edit/?id=%@&project_id=%@",referenceID,newProjectID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setValue:[NSString stringWithFormat:@"application/json"] forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:@{
                                                                   @"project_id":referenceType,
                                                                   } options:(NSJSONWritingOptions)0 error:&error]];

    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(switchSuccessfull)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate switchSuccessfull];
                     }];
                 }
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(switchFailedWithError:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate switchFailedWithError:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(switchFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate switchFailedWithError:@"Connection error"];
                 }];
             }
             
         }

         
         
     }];

    

}

-(void)editReferenceWithData:(NSString *)dataString referenceType:(NSString *)refType projectID:(NSString *)projectID andReferenceID:(NSString *)referenceID
{
    NSError * error = nil;
    NSString *parameterString = [NSString stringWithFormat:@"references/edit/%@",referenceID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setValue:[NSString stringWithFormat:@"application/json"] forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:@{
                                                                   @"data":dataString,
                                                                   @"project_id":projectID,
                                                                   @"reference_type":refType,
                                                                   } options:(NSJSONWritingOptions)0 error:&error]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(referenceSucessfullyEdited:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate referenceSucessfullyEdited:[parsedObject objectForKey:@"data"]];
                     }];
                 }
             }
             else
             {
                 if( [_delegate respondsToSelector:@selector(referenceEditingFailedWithErrror:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate referenceEditingFailedWithErrror:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(referenceEditingFailedWithErrror:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate referenceEditingFailedWithErrror:@"Connection error"];
                 }];
             }
             
         }

         
         
     }];

}


-(void)createReferenceWithData:(NSString *)dataString referenceType:(NSString *)refType andProjectID:(NSString *)projectID
{
    

    NSError * error = nil;
    NSString *parameterString = [NSString stringWithFormat:@"references/create/"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setValue:[NSString stringWithFormat:@"application/json"] forHTTPHeaderField:@"Content-Type"];

    
    NSLog(@"%@",projectID);
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:@{
                                                                   @"data":dataString,
                                                                   @"project_id":projectID,
                                                                   @"reference_type":refType,
                                                                   } options:(NSJSONWritingOptions)0 error:&error]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0 && parsedObject != nil) // Sucess
             {
                 if( [_delegate respondsToSelector:@selector(referenceSucessfullyCreated:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate referenceSucessfullyCreated:[parsedObject objectForKey:@"data"]];
                     }];
                 }
             }
             else if (parsedObject != nil) {
                 if( [_delegate respondsToSelector:@selector(referenceCreationFailedWithEror:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate referenceCreationFailedWithEror:[parsedObject objectForKey:@"errorMessages"]];
                     }];
                 }
             }
             else {
                 if( [_delegate respondsToSelector:@selector(referenceCreationFailedWithEror:)])
                 {
                     [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                         [_delegate referenceCreationFailedWithEror:@"Unknown error"];
                     }];
                 }
             }
             
         }
         else
         {
             if( [_delegate respondsToSelector:@selector(referenceCreationFailedWithEror:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate referenceCreationFailedWithEror:@"Connection error"];
                 }];
             }
             
         }

         
         
     }];
    
}


-(void)getReferencesForProjectID:(NSString *)projectID andProjectToAdd:(Project *)projectToAdd
{
    NSString *parameterString = [NSString stringWithFormat:@"references/?project_id=%@",projectID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];

             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 NSArray *refArray = [parsedObject objectForKey:@"data"];
                 
                 for (NSDictionary *refDict in refArray)
                 {
                     AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                     NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
                     
                     
                     NSEntityDescription *entity = [NSEntityDescription entityForName:@"Reference" inManagedObjectContext:managedObjectContext];
                     Reference *newRef = [[Reference alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
                     
                     newRef.projectID = [[refDict objectForKey:@"project_id"] stringValue];
                     newRef.referenceType = [refDict objectForKey:@"reference_type"];
                     newRef.data = [refDict objectForKey:@"data"];
                     newRef.referenceID = [[refDict objectForKey:@"id"] stringValue];
                     
                     NSString *dateString = [refDict objectForKey:@"created_at"];
                     dateString = [dateString substringToIndex:[dateString length]-5];
                     
                     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                     [dateFormat setDateFormat:@"yyyy-M-d'T'H:m:s"];
                     
                     newRef.dateCreated = [dateFormat dateFromString:dateString];

                     [projectToAdd addReferencesObject:newRef];
                 }
                 
                 _projectsLeftToSync --;
                 
                 if (_projectsLeftToSync == 0)
                 {
                     if( [_delegate respondsToSelector:@selector(userSyncComplete)])
                     {
                         [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                             [_delegate userSyncComplete];
                         }];
                     }
                 }
            }
           else if ([_delegate respondsToSelector:@selector(loginFailedWithError:)]) {
             [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
               [_delegate loginFailedWithError:@"Connection error"];
             }];
           }
         }
         else if ( [_delegate respondsToSelector:@selector(loginFailedWithError:)]) {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate loginFailedWithError:@"Connection error"];
                 }];
         }
     }];

    
}


-(void)getUserData
{
    NSString *parameterString = [NSString stringWithFormat:@"users/projects/%@",_user.userID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAPIBaseURLString,parameterString]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         
         if (connectionError == nil)
         {
             NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
             dataString = [dataString stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
             
             NSData *newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
             NSError *error = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:newData options:0 error:&error];
             NSLog(@"%@",parsedObject);
             
             int errorCode = [[parsedObject objectForKey:@"errorCode"] intValue];
             
             if (errorCode == 0) // Sucess
             {
                 
                 _onlineProjectsArray = [[NSMutableArray alloc] init];
                 
                 NSArray *projectsArray = [parsedObject objectForKey:@"data"];
                 AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                 NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;

                 _projectsLeftToSync = [projectsArray count];
                 
                 for (NSDictionary *projectDict in projectsArray)
                 {
                     NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:managedObjectContext];
                     Project *newProject = [[Project alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];

                     newProject.onlineProject = [NSNumber numberWithBool:TRUE];
                     newProject.name = [projectDict objectForKey:@"name"];
                     newProject.projectID = [[projectDict objectForKey:@"id"] stringValue];
                     
                     
                     [self getReferencesForProjectID:newProject.projectID andProjectToAdd:newProject];
                     
            
                     [_onlineProjectsArray addObject:newProject];
                    
                     
                 }
             }
             else if( [_delegate respondsToSelector:@selector(loginFailedWithError:)])
             {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                  [_delegate loginFailedWithError:@"Connection error"];
                }];
             }

         }
         else
         {
             if( [_delegate respondsToSelector:@selector(loginFailedWithError:)])
             {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                     [_delegate loginFailedWithError:@"Connection error"];
                 }];
             }
             
         }

     }];
}

#pragma mark WORDCAT METHODS


- (void) searchBooksWithISBN:(NSString *)reqString {
    
    
    reqString = [reqString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.worldcat.org/webservices/catalog/search/worldcat/opensearch?q=%@&wskey=%@",reqString,kWorldCatAPIKey]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
    
    dataBuffer = nil;
    searchConnection = [NSURLConnection connectionWithRequest:request delegate:self];
}

-(void)getBookWithIdentifier:(NSString *)identifier
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.worldcat.org/webservices/catalog/content/%@?wskey=%@",identifier,kWorldCatAPIKey]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
    
    dataBuffer = nil;
    getConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"RESPONSE %@",response);
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int code = [httpResponse statusCode];
    
    if (code != 200)
    {
        if( [_delegate respondsToSelector:@selector(searchBooksFailedWithError:)])
        {
            [_delegate searchBooksFailedWithError:@"Could not get book information"];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSError *error;
    NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:data options:XMLReaderOptionsProcessNamespaces error:&error];
    
    
    if (xmlDictionary != nil)
    {
        if (connection == searchConnection)
            [self parseSearchResponseWithDictionary:xmlDictionary];
        else
            [self parseGetResponseWithDictionary:xmlDictionary];
    }
    else
    {
        if (dataBuffer == nil)
        {
            dataBuffer = [NSMutableData data];
            [dataBuffer appendData:data];
        }
        else
        {
            [dataBuffer appendData:data];
            
            NSString *dataString = [[NSString alloc] initWithData:dataBuffer encoding:NSUTF8StringEncoding];
            NSLog(@"!->%@",dataString);
            
            
            NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:dataBuffer options:XMLReaderOptionsProcessNamespaces error:&error];
            
            if (xmlDictionary != nil)
            {
                if (connection == searchConnection)
                    [self parseSearchResponseWithDictionary:xmlDictionary];
                else
                    [self parseGetResponseWithDictionary:xmlDictionary];
                
                return;
            }
            
            
            //            dataBuffer = nil;
            //
            //            if( [_delegate respondsToSelector:@selector(searchBooksFailedWithError:)])
            //            {
            //                [_delegate searchBooksFailedWithError:@"Could not get book information"];
            //            }
        }
    }
}

-(void)parseGetResponseWithDictionary:(NSDictionary *)xmlDictionary
{
    @try{
        
        xmlDictionary = [xmlDictionary objectForKey:@"record"];
        NSDictionary *typeDictionary;
        NSMutableDictionary *refDataDictionary = [[NSMutableDictionary alloc] init];
        NSMutableArray *authorArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dict in _typeData)
        {
            if ([[dict objectForKey:@"name"] isEqualToString:@"Book"])
            {
                typeDictionary = dict;
                
                for (NSString *fields in [dict objectForKey:@"fields"])
                {
                    if ([fields isEqualToString:@"Accessed Date"])
                    {
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateStyle = NSDateFormatterLongStyle;
                        formatter.timeStyle = NSDateFormatterNoStyle;
        
                        NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
                        [refDataDictionary setObject:stringFromDate forKey:fields];
                    }
                    else
                        [refDataDictionary setObject:@"" forKey:fields];
                }
            }
        }
        
        NSArray *dataFields = [xmlDictionary objectForKey:@"datafield"];
        
        for (NSDictionary  *dataDict in dataFields)
        {
            if([self authorshipTagFound:[[dataDict objectForKey:@"tag"] intValue]])
            {
                NSArray *subfields;
                if ([[dataDict objectForKey:@"subfield"] isKindOfClass:[NSDictionary class]]){
                    subfields = @[[dataDict objectForKey:@"subfield"]];
                }
                else{
                    subfields = [dataDict objectForKey:@"subfield"];
                }
                
                for (NSDictionary  *subfieldDict in subfields){
                    
                    if ([[subfieldDict objectForKey:@"code"] isEqualToString:@"a"]){
                        
                        NSString *processedString = [self removeCharacters:@[@",",@"."] FromString:[subfieldDict objectForKey:@"text"]];
        
                        NSArray *nameArray = [processedString componentsSeparatedByString:@" "];
                        NSString *initialsString = @"";
                  
                        for (int i = 1; i < [nameArray count]; i ++)
                        {
                            if (i > 1)
                                initialsString = [initialsString stringByAppendingString:@" "];
                            
                            initialsString = [initialsString stringByAppendingString:[[nameArray objectAtIndex:i] substringToIndex:1]];
                        }
                        [authorArray addObject:@{@"Surname":[nameArray objectAtIndex:0],@"Initials":initialsString}];
                    }
                }
            }
            
            if([self citationTagFound:[[dataDict objectForKey:@"tag"] intValue]])   {
                NSArray *subfields;
                if ([[dataDict objectForKey:@"subfield"] isKindOfClass:[NSDictionary class]]){
                    subfields = @[[dataDict objectForKey:@"subfield"]];
                }
                else{
                    subfields = [dataDict objectForKey:@"subfield"];
                }
                
                for (NSDictionary  *subfieldDict in subfields){
                    
                    if ([[subfieldDict objectForKey:@"code"] isEqualToString:@"a"]){
                        [refDataDictionary setObject:[self removeCharacters:@[@" :"] FromString:[subfieldDict objectForKey:@"text"]] forKey:@"Place of Publication"];
                    }
                
                    if ([[subfieldDict objectForKey:@"code"] isEqualToString:@"b"]){
                        [refDataDictionary setObject:[self removeCharacters:@[@","] FromString:[subfieldDict objectForKey:@"text"]] forKey:@"Publisher"];
                    }
                    
                    if ([[subfieldDict objectForKey:@"code"] isEqualToString:@"c"]){
                        [refDataDictionary setObject:[self removeCharacters:@[@".",@"c",@"©"] FromString:[subfieldDict objectForKey:@"text"]] forKey:@"Year"];
                    }
                }
            }
            
            if([self titleTagFound:[[dataDict objectForKey:@"tag"] intValue]])
            {
                NSArray *subfields;
                
                if ([[dataDict objectForKey:@"subfield"] isKindOfClass:[NSDictionary class]])
                {
                    subfields = @[[dataDict objectForKey:@"subfield"]];
                }
                else
                {
                    subfields = [dataDict objectForKey:@"subfield"];
                }
                
                for (NSDictionary  *subfieldDict in subfields){
                    
                    if ([[subfieldDict objectForKey:@"code"] isEqualToString:@"a"]){
                        
                        [refDataDictionary setObject:[self removeCharacters:@[@" :"] FromString:[subfieldDict objectForKey:@"text"]] forKey:@"Title"];
                    }
                }
            }
        }
        
        if ([authorArray count] == 0) {
            
            [authorArray addObject:@{@"Surname":@"",@"Initials":@""}];
            
        }
        
        [refDataDictionary setObject:authorArray forKey:@"Author Double Field"];
        [refDataDictionary setObject:[NSNumber numberWithInt:0]  forKey:@"hasEditors"];
       
        NSError *error;
        NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:refDataDictionary options:kNilOptions error:&error] encoding:NSUTF8StringEncoding];
        
        if ([_delegate respondsToSelector:@selector(getBookSucessfullWithData:)]){
            
            [_delegate getBookSucessfullWithData:dataString];
        }
    }
    
    @catch (NSException *exception)
    {
        if( [_delegate respondsToSelector:@selector(searchBooksFailedWithError:)]){
            
            [_delegate searchBooksFailedWithError:@"Could not parse book information"];
        }
    }
}

-(void)parseSearchResponseWithDictionary:(NSDictionary *)xmlDictionary
{
    @try
    {
        xmlDictionary = [xmlDictionary objectForKey:@"feed"];
        NSArray *entryArray;
        
        if ([[xmlDictionary objectForKey:@"entry"] isKindOfClass:[NSDictionary class]])
            entryArray = @[[xmlDictionary objectForKey:@"entry"]];
        else
            entryArray = [xmlDictionary objectForKey:@"entry"];
        
        if( [_delegate respondsToSelector:@selector(searchBooksSucessfullWithData:)])
        {
            
            //Your code goes in here
            [_delegate searchBooksSucessfullWithData:entryArray];
            
            
            
        }
    }
    @catch (NSException *exception)
    {
        if( [_delegate respondsToSelector:@selector(searchBooksFailedWithError:)])
        {
            
            //Your code goes in here
            [_delegate searchBooksFailedWithError:@"Could not parse book information"];
            
        }
    }

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"ERROR %@",error);
    
    if( [_delegate respondsToSelector:@selector(searchBooksFailedWithError:)])
    {
        [_delegate searchBooksFailedWithError:@"Could not get book information"];
    }
}




#pragma mark Utility methods

-(NSString *)removeCharacters:(NSArray *)charactersToRemove FromString:(NSString *)string
{
    
    for (NSString *character in charactersToRemove)
    {
        string = [string stringByReplacingOccurrencesOfString:character withString:@""];
    }
    
    return string;
}

- (BOOL) titleTagFound:(int) tagVal{

    BOOL isTagFound = FALSE;
    
    if(tagVal == 222 || tagVal == 240 || tagVal == 243 || tagVal == 245 || tagVal == 246 || tagVal == 730 || tagVal == 740){
        isTagFound = TRUE;
    }
    return isTagFound;
}

- (BOOL) citationTagFound:(int) tagVal{
    
    BOOL isTagFound = FALSE;
    
    if(tagVal == 250 || tagVal == 254 || tagVal == 255 || tagVal == 260 || tagVal == 261 || tagVal == 262 || tagVal == 263 || tagVal == 264 || tagVal == 300 || tagVal == 502 || tagVal == 773 || tagVal == 856){
        isTagFound = TRUE;
    }
    return isTagFound;
}

- (BOOL) authorshipTagFound:(int) tagVal{
    
    BOOL isTagFound = FALSE;
    
    if(tagVal == 100 || tagVal == 700 || tagVal == 110 || tagVal == 710 || tagVal == 111 || tagVal == 711 || tagVal == 720 || tagVal == 130){
        isTagFound = TRUE;
    }
    return isTagFound;
}

@end
