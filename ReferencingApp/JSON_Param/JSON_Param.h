//  JSON_Param_ViewController.h
//  Harvard
//  Created by appentus technologies pvt. ltd. on 10/25/19.
//  Copyright © 2019 SquaredDimesions. All rights reserved.

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

//NS_ASSUME_NONNULL_BEGIN

@interface JSON_Param : NSObject

+(id)sharedInstance;

-(NSArray*)func_JSON_Param;
-(NSDictionary*)func_JSON_Type;

-(void)func_selected_tabbar:(NSString *)str_selected_tabbar;

@property (strong,nonatomic) NSString *str_selected_tabbar;

@end



//NS_ASSUME_NONNULL_END
