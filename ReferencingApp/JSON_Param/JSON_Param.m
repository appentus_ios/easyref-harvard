//  JSON_Param_ViewController.m
//  Harvard
//  Created by appentus technologies pvt. ltd. on 10/25/19.
//  Copyright © 2019 SquaredDimesions. All rights reserved.

#import "JSON_Param.h"

static JSON_Param *sharedInstance;

@interface JSON_Param()

@end

@implementation JSON_Param

+(id)sharedInstance {
    static JSON_Param *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        // write your code here
    }
    return self;
}

-(NSArray*)func_JSON_Param {
    return @[@"id",@"type",@"issued",@"title",@"medium",@"URL",@"accessed",@"note",
             @"container-title",
             @"edition",@"publisher",@"publisher-place",@"number-of-pages",
             @"source",
             @"page",
             @"container",
             @"collection-title"];
}

-(NSDictionary*)func_JSON_Type {
    return @{@"artwork":@"graphic",
             @"blog post":@"post-weblog",
             @"book":@"book",
             @"book chapter":@"chapter",
             @"conference paper":@"paper-conference",
             @"database":@"dataset",
             @"dictionary entry":@"entry",
             @"e-book":@"book",
             @"encyclopedia entry":@"entry-encyclopedia",
             @"film/movie":@"motion_picture",
             @"image":@"container-title",
             @"interview":@"interview",
             @"journal article":@"article-journal",
             @"legal bill":@"bill",
             @"legislation":@"legislation",
             @"legal case":@"legal_case",
             @"magazine article":@"article-magazine",
             @"map":@"map",
             @"news article":@"article-newspaper",
             @"patent":@"patent",
             @"report":@"report",
             @"review":@"review",
             @"song":@"song",
             @"speech":@"speech",
             @"standard":@"book",
             @"thesis":@"thesis",
             @"video":@"motion_picture",
             @"website":@"webpage",
             @"book review":@"review-book"
             };
}

-(void)func_selected_tabbar:(NSString *)str_selected_tabbar {
    self.str_selected_tabbar = str_selected_tabbar;
}

@end


